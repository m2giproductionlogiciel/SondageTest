# Projet de Production Logicielle - Projet Universitaire
Bienvenue dans le projet de Sondage de Date en Ligne ! Ce projet a été réalisé par une équipe de 4 membres talentueux pendant une période de 2 mois, dans le cadre du Master 2 Génie Informatique à l'UFR MIM de Metz. L'équipe était composée de Gaetan Korpys, Théo Rousseau, Guillaume Vautrin et Catherine Hyunh.

## Présentation du Projet
L'objectif principal de ce projet était de mettre en place une application de sondage de date en ligne, permettant aux utilisateurs de créer, modifier et participer à des sondages pour trouver la meilleure date possible. Le projet reposait sur une API REST préexistante qui nous a été fournie. Notre mission consistait à renforcer la robustesse de l'application en mettant en place des tests unitaires, d'intégration et end-to-end, ainsi qu'à établir une pipeline de CI/CD pour faciliter le déploiement continu.

## Mise en Production et Tests
### Tests
Nous avons mis en place une suite complète de tests pour garantir la fiabilité de l'application :

Tests Unitaires : Des tests unitaires ont été créés pour chaque composant individuel de l'application, garantissant le bon fonctionnement des parties isolées.

Tests d'Intégration : Les tests d'intégration ont permis de vérifier la cohérence des interactions entre les différentes parties de l'application.

Tests End-to-End : Les tests end-to-end ont été réalisés pour simuler le parcours complet de l'utilisateur, de la création d'un sondage à la participation.

### Pipeline de CI/CD
Une pipeline de CI/CD complète a été mise en place pour automatiser le processus d'intégration et de déploiement continu. Les étapes comprenaient :

Linter/Formatter : Un linter et un formatter ont été intégrés pour assurer la cohérence du code.

Tests Automatisés : La pipeline a été configurée pour déclencher automatiquement les tests unitaires, d'intégration et end-to-end à chaque modification du code.

Déploiement dans un Environnement Public : La dernière version stable de l'application est automatiquement déployée dans un environnement public accessible.

## Documentation
La documentation générale de notre travail, ainsi que la documentation technique détaillée, se trouvent dans la partie Wiki de ce dépôt GitLab. Vous y trouverez des informations détaillées sur l'API REST, la configuration de la pipeline, les choix architecturaux et d'autres aspects cruciaux de notre travail.

Nous sommes fiers du résultat de notre collaboration et espérons que cette application de sondage de date en ligne sera utile et efficace pour les utilisateurs finaux.

N'hésitez pas à explorer la documentation dans le Wiki pour des informations plus détaillées sur notre projet. Si vous avez des questions ou des commentaires, n'hésitez pas à nous contacter.

Merci pour votre intérêt dans notre projet !
