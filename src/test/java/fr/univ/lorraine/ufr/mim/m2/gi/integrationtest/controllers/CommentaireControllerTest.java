package fr.univ.lorraine.ufr.mim.m2.gi.integrationtest.controllers;

import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.MySurveyApplication;
import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.exceptions.BadRequestException;
import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.exceptions.NotFoundException;
import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.models.Commentaire;
import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.models.Participant;
import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.models.Sondage;
import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.services.CommentaireService;
import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.services.ParticipantService;
import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.services.SondageService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(classes = MySurveyApplication.class)
@AutoConfigureMockMvc
@TestPropertySource(locations = "classpath:application-integrationtest.properties")
@Transactional
public class CommentaireControllerTest {
    @Autowired
    private CommentaireService commentaireService;

    @Autowired
    private SondageService sondageService;

    @Autowired
    private ParticipantService participantService;

    @Autowired
    private MockMvc mockMvc;

    Long commentaireId;

    Long participantId;


    @BeforeEach
    public void setUp() throws BadRequestException, NotFoundException {
        Participant participant = new Participant();
        participant.setParticipantId(1L);
        participant.setNom("Rousseau");
        participant.setPrenom("Théo");
        participant = participantService.create(participant);
        participantId = participant.getParticipantId();

        Sondage sondage = new Sondage();
        sondage.setSondageId(1L);
        sondage.setNom("Test Survey");
        sondage.setDescription("This is a test survey");
        sondage = sondageService.create(participant.getParticipantId(), sondage);
        participantId = participant.getParticipantId();

        Commentaire commentaire = new Commentaire();
        commentaire.setCommentaireId(1L);
        commentaire.setCommentaire("Test");
        commentaire.setParticipant(participant);
        commentaire.setSondage(sondage);
        commentaire = commentaireService.addCommantaire(sondage.getSondageId(), participant.getParticipantId(), commentaire);
        commentaireId = commentaire.getCommentaireId();
    }

    @Test
    public void GivenExistingCommentaireWhenUpdateThenCorrectlyUpdated() throws Exception
    {
        mockMvc.perform(put("/api/commentaire/{id}", commentaireId)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{" +
                                "\"commentaireId\": \"" +  commentaireId + "\",\n" +
                                "\"commentaire\": \"Nouveau test\",\n" +
                                "\"participant\": \"" +  participantId + "\"\n" +
                                "}")
                )
                .andExpect(jsonPath("$.commentaire").value("Nouveau test"))
                .andExpect(jsonPath("$.participant").value(participantId))
                .andExpect(status().isOk());
    }

    @Test
    public void GivenUnknownParticipantWhenUpdateThenNotFound() throws Exception
    {

        mockMvc.perform(put("/api/commentaire/{id}", 200000000L)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{" +
                                "\"commentaireId\": \"" +  commentaireId + "\",\n" +
                                "\"commentaire\": \"Nouveau test\",\n" +
                                "\"participant\": \"" +  participantId + "\"\n" +
                                "}")
                )
                .andExpect(status().isNotFound());
    }

    @Test
    public void GivenEmptyCommentaireWhenUpdateThenBadRequest() throws Exception
    {

        mockMvc.perform(put("/api/commentaire/{id}", 1L)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{" +
                                "\"commentaireId\": \"" +  commentaireId + "\",\n" +
                                "\"commentaire\": \"\",\n" +
                                "\"participant\": \"\"\n" +
                                "}")
                )
                .andExpect(jsonPath("$.message").value("Requête malformée. Veuillez fournir le champ commentaire avec des valeurs non nuls."))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void GivenExistingParticipantWhenDeleteParticipantThenDeleted() throws Exception {
        mockMvc.perform(delete("/api/commentaire/{id}", 1L))
                .andExpect(status().isOk());
    }
    @Test
    public void GivenUnknownParticipantWhenDeleteThenError() throws Exception {
        mockMvc.perform(delete("/api/commentaire/{id}", 2L))
                .andExpect(status().isOk());
    }
}

