package fr.univ.lorraine.ufr.mim.m2.gi.endtoendtest;

import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.MySurveyApplication;
import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.dtos.CommentaireDto;
import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.dtos.SondageDto;
import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.exceptions.BadRequestException;
import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.exceptions.NotFoundException;
import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.models.Commentaire;
import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.models.Participant;
import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.models.Sondage;
import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.repositories.SondageRepository;
import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.services.CommentaireService;
import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.services.ParticipantService;
import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.services.SondageService;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.TestPropertySource;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.*;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, classes = MySurveyApplication.class)
@TestPropertySource(locations = "classpath:application-integrationtest.properties")
public class CommentaireControllerEndToEndTest {

    @Autowired
    private ParticipantService participantService;
    @Autowired
    private SondageService sondageService;
    @Autowired
    private CommentaireService commentaireService;

    private final String NOM = "NOM";
    private final String PRENOM = "PRENOM";
    private final String DESCRIPTION = "DESCRIPTION";
    private final boolean CLOTURE = false;
    private final Date DATE = Date.from(LocalDate.now().atStartOfDay(ZoneId.systemDefault()).toInstant());
    private final String COMMENTAIRE = "COMMENTAIRE";
    private CommentaireDto commentaireDto;
    private Long participantId;
    private Long sondageId;
    private Long commentaireId;

    @LocalServerPort
    private int port;

    @BeforeEach
    public void setUp() throws BadRequestException, NotFoundException {

        // -- Création et insertion du Participant
        // -------------------------------------------
        Participant participant = new Participant();
        participant.setNom(NOM);
        participant.setPrenom(PRENOM);
        participant = participantService.create(participant);
        participantId = participant.getParticipantId();


        // -- Création et insertion du Sondage
        // ---------------------------------------
        Sondage sondage = new Sondage();
        sondage.setNom(NOM);
        sondage.setDescription(DESCRIPTION);
        sondage.setCloture(CLOTURE);
        sondage.setFin(DATE);

        sondageService.create(participant.getParticipantId(), sondage);
        sondageId = sondage.getSondageId();

        // -- Création et insertion du Commentaire
        // ---------------------------------------
        Commentaire commentaire= new Commentaire();
        commentaire.setCommentaire(COMMENTAIRE);
        commentaire.setParticipant(participant);

        commentaireService.addCommantaire(sondageId, participantId, commentaire);
        commentaireId = commentaire.getCommentaireId();

        commentaireDto = new CommentaireDto();
        commentaireDto.setCommentaireId(commentaireId);
        commentaireDto.setCommentaire(COMMENTAIRE);
        commentaireDto.setParticipant(participantId);

    }

    //region GetById
    @Test
    void GivenACommentaireId_whenGetCommentaireById_thenCheckCommentaire() {
        RestAssured.port = port;

        Response response = given()
                .contentType(ContentType.JSON)
                .get("/api/sondage/{id}/commentaires", sondageId);

        response.then()
                .statusCode(HttpStatus.OK.value()).extract()
                .body()
                .jsonPath().getList(".", CommentaireDto.class).get(0).equals(commentaireDto);
    }

    //endregion

    //region Create
    @Test
    void GivenACommentaire_whenCreateCommentaire_thenCheckCommentaireIsValid() {
        RestAssured.port = port;

        String sondageName = "Test Survey";
        String sondageDesc = "This is a test survey";

        Response response = given()
                .contentType(ContentType.JSON)
                .body(commentaireDto)
                .post("/api/sondage/{id}/commentaires", sondageId);

        response.then().statusCode(201);
    }

    //endregion

    //region Update
    @Test
    void GivenCommentaireId_whenUpdateCommentaire_thenCheckUpdatedCommentaire() {
        RestAssured.port = port;

        Response response = given()
                .contentType(ContentType.JSON)
                .body(commentaireDto)
                .put("/api/commentaire/{id}", commentaireId);

        response.then().statusCode(HttpStatus.OK.value())
                .body("commentaireId", equalTo(commentaireId.intValue()))
                .body("commentaire", equalTo(COMMENTAIRE))
                .body("participant", equalTo(participantId.intValue()));
    }


    //endregion

    //region Delete
    @Test
    void GivenCommentaireId_whenDeleteCommentaire_thenCheckIfCommentaireIsDeleted() {
        RestAssured.port = port;

        given()
                .contentType(ContentType.JSON)
                .when()
                .delete("/api/commentaire/{id}", commentaireId)
                .then()
                .statusCode(HttpStatus.OK.value());
    }
}
