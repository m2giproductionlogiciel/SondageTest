package fr.univ.lorraine.ufr.mim.m2.gi.unittest.services;

import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.exceptions.BadRequestException;
import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.exceptions.NotFoundException;
import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.models.DateSondage;
import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.models.Sondage;
import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.repositories.DateSondageRepository;
import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.services.DateSondageService;
import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.services.SondageService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

class DateSondageServiceTest {

    @Mock
    private DateSondageRepository dateSondageRepository;

    @Mock
    private SondageService sondageService;

    @InjectMocks
    private DateSondageService dateSondageService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void getById() throws NotFoundException {
        // Créer un objet DateSondage fictif
        DateSondage expectedDateSondage = new DateSondage();
        when(dateSondageRepository.findById(1L)).thenReturn(Optional.of(expectedDateSondage));

        // Appeler la méthode getById du service
        DateSondage actualDateSondage = dateSondageService.getById(1L);

        // Vérifier que la méthode du repository a été appelée avec le bon ID
        verify(dateSondageRepository, times(1)).findById(1L);

        // Vérifier que l'objet renvoyé par le service est le même que celui renvoyé par le repository
        assertEquals(expectedDateSondage, actualDateSondage);
    }

    @Test
    void givenValidDateId_whenGetById_thenDateSondageReturned() throws NotFoundException {
        // Given
        DateSondage dateSondage = new DateSondage();
        dateSondage.setDateSondageId(1L);
        dateSondage.setDate(new Date());

        when(dateSondageRepository.findById(1L)).thenReturn(Optional.of(dateSondage));

        // When
        DateSondage result = dateSondageService.getById(1L);

        // Then
        assertEquals(dateSondage, result);
    }

    @Test
    void getBySondageId() {
        // Créer une liste fictive de DateSondage
        List<DateSondage> expectedDateSondageList = new ArrayList<>();
        when(dateSondageRepository.getAllBySondage(1L)).thenReturn(expectedDateSondageList);

        // Appeler la méthode getBySondageId du service
        List<DateSondage> actualDateSondageList = dateSondageService.getBySondageId(1L);

        // Vérifier que la méthode du repository a été appelée avec le bon ID
        verify(dateSondageRepository, times(1)).getAllBySondage(1L);

        // Vérifier que la liste renvoyée par le service est la même que celle renvoyée par le repository
        assertEquals(expectedDateSondageList, actualDateSondageList);
    }

    @Test
    void create() throws BadRequestException, NotFoundException {
        // Créer un objet DateSondage fictif
        DateSondage dateSondageToCreate = new DateSondage();
        dateSondageToCreate.setDateSondageId(1L);

        // Créer un objet Sondage fictif
        Sondage sondage = new Sondage();
        when(sondageService.getById(1L)).thenReturn(sondage);

        // Mocker la méthode save de repository
        when(dateSondageRepository.save(any())).thenReturn(dateSondageToCreate);

        // Appeler la méthode create du service
        DateSondage createdDateSondage = dateSondageService.create(1L, new DateSondage());

        // Vérifier que la méthode du repository a été appelée pour enregistrer le nouvel objet
        verify(dateSondageRepository, times(1)).save(any());

        // Vérifier que l'objet renvoyé par le service est le même que celui renvoyé par le repository
        assertEquals(dateSondageToCreate, createdDateSondage);
    }

    @Test
    void delete() throws NotFoundException {
        // Simuler la présence de l'objet dans le repository
        when(dateSondageRepository.findById(1L)).thenReturn(java.util.Optional.of(new DateSondage()));

        // Appeler la méthode delete du service
        int result = dateSondageService.delete(1L);

        // Vérifier que la méthode du repository a été appelée pour supprimer l'objet
        verify(dateSondageRepository, times(1)).deleteById(1L);

        // Vérifier que la méthode renvoie 1, indiquant que la suppression a réussi
        assertEquals(1, result);
    }
}
