package fr.univ.lorraine.ufr.mim.m2.gi.unittest.services;

import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.exceptions.BadRequestException;
import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.exceptions.NotFoundException;
import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.models.Commentaire;
import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.models.Participant;
import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.models.Sondage;
import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.repositories.CommentaireRepository;
import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.services.CommentaireService;
import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.services.ParticipantService;
import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.services.SondageService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import javax.servlet.http.Part;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.same;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class CommentaireServiceTest {
    @Mock
    private  CommentaireRepository commentaireRepository;

    @Mock
    private  SondageService sondageService;

    @Mock
    private  ParticipantService participantService;

    @InjectMocks
    private CommentaireService commentaireService;

    @Test
    void givenACommentaire_whenCreateCommentaire_thenCommentaireSondadeAndParticipantServicesAreCalled() throws BadRequestException, NotFoundException {
        // Given
        Commentaire commentaire = mock(Commentaire.class);

        // When
        commentaireService.addCommantaire(1L, 1L, commentaire);

        // Then
        verify(sondageService, times(1)).getById(1L);
        verify(participantService, times(1)).getById(1L);
        verify(commentaireRepository, times(1)).save(same(commentaire));
    }

    @Test
    void givenACommentaire_whenGetBySondageId_thenCommentaireRepositoryIsCalled() {
        //Given
        Commentaire commentaire = mock(Commentaire.class);

        //When
        commentaireService.getBySondageId(1L);

        //Then
        verify(commentaireRepository, times(1)).getAllBySondage(1L);
    }

    @Test
    void givenACommentaire_whenGetCommentaireUpdated_thenCommentaireRepositoryIsCalled() throws BadRequestException, NotFoundException {
        //Given
        Commentaire commentaire = mock(Commentaire.class);
        Participant participant = mock(Participant.class);
        Sondage sondage = mock(Sondage.class);
        commentaire.setParticipant(participant);
        commentaire.setSondage(sondage);


        // Mocking repository response
        when(commentaireRepository.findById(1L)).thenReturn(Optional.of(commentaire));
        when(commentaireRepository.getById(1L)).thenReturn(commentaire);

        //When
        commentaireService.update(1L, 1L, commentaire);

        //Then
        verify(commentaireRepository, times(1)).findById(1L);
        verify(commentaireRepository, times(1)).save(same(commentaire));
    }

    @Test
    void givenACommentaire_whenGetCommentaireDeleted_thenCommentaireRepositoryIsCalled() {
        //Given
        Commentaire commentaire = mock(Commentaire.class);

        // Mocking repository response
        when(commentaireRepository.findById(1L)).thenReturn(Optional.of(commentaire));

        //When
        commentaireService.delete(1L);

        //Then
        verify(commentaireRepository, times(1)).findById(1L);
        verify(commentaireRepository, times(1)).deleteById(1L);
    }
}
