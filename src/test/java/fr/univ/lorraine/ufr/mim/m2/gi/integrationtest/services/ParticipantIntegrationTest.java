package fr.univ.lorraine.ufr.mim.m2.gi.integrationtest.services;

import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.MySurveyApplication;
import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.exceptions.BadRequestException;
import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.exceptions.NotFoundException;
import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.models.Participant;
import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.services.ParticipantService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.test.context.TestPropertySource;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


@SpringBootTest(classes = MySurveyApplication.class)
@AutoConfigureMockMvc
@TestPropertySource(locations = "classpath:application-integrationtest.properties")
@Transactional
class ParticipantIntegrationTest {
    @Autowired
    private ParticipantService participantService;

    private final String NOM = "NOM";
    private final String PRENOM = "PRENOM";

    private Participant savedParticipant;

    @BeforeEach
    public void setUp(){
        Participant participant = new Participant();
        participant.setNom(NOM);
        participant.setPrenom(PRENOM);
        savedParticipant = participantService.create(participant);
    }

    @Test
    void GivenParticipant_WhenSaved_ThenCorrectlySaved() throws BadRequestException, NotFoundException {
        Long id = savedParticipant.getParticipantId();
        Participant p = participantService.getById(savedParticipant.getParticipantId());
        Assertions.assertEquals(NOM, p.getNom(), "Le nom devrait être le même.");
        Assertions.assertEquals(PRENOM, p.getPrenom(), "Le prénom devrait être identique");
        Assertions.assertEquals(id, p.getParticipantId(), "L'identifiant devrait être le même.");
    }

    @Test
    void GivenParticipant_WhenSavedWithNull_ThenCorrectlySaved(){
        Participant temp = new Participant();
        Assertions.assertThrows(IllegalArgumentException.class, () -> temp.setNom(null), "Le nom ne doit pas être vide");
        Assertions.assertThrows(IllegalArgumentException.class, () -> temp.setPrenom(null), "Le prénom ne doit pas être vide");
        Assertions.assertThrows(DataIntegrityViolationException.class, () -> participantService.create(temp), "La création doit être rejetée car nom et prénom vide");
    }

    @Test
    void GivenParticipantSaved_WhenGetAll_ThenGetAll(){
        List<Participant> participants = participantService.getAll();
        Assertions.assertFalse(participants.isEmpty());
    }

    @Test
    void GivenParticipant_WhenUpdated_ThenCorrectlyUpdated(){
        Long id = savedParticipant.getParticipantId();
        Participant updatedParticipant = savedParticipant;
        updatedParticipant.setNom("TEST");
        Participant p = participantService.update(id, updatedParticipant);
        Assertions.assertEquals("TEST", p.getNom(), "La mise à jour n'a pas été correctement faite");
        Assertions.assertEquals(id, p.getParticipantId(), "L'objet modifié n'a pas le bon identifiant");
    }

    @Test
    void GivenParticipant_WhenUpdatedWithIncorrectDataWithNom_ThenErrorOnUpdate(){
        Long id = savedParticipant.getParticipantId();
        Participant updatedParticipant = savedParticipant;
        Assertions.assertThrows(IllegalArgumentException.class, () -> savedParticipant.setNom(null), "Une IllegalArgumentException devrait être déclenchée car le nom est vide.");
        Participant p = participantService.update(id, updatedParticipant);
        Assertions.assertEquals(id, p.getParticipantId(), "L'objet modifié n'a pas le bon identifiant");
        Assertions.assertEquals(NOM, p.getNom(), "L'objet n'aurait pas dû être modifié.");
    }

    @Test
    void GivenParticipant_WhenUpdatedWithIncorrectDataWithPrenom_ThenErrorOnUpdate(){
        Long id = savedParticipant.getParticipantId();
        Participant updatedParticipant = savedParticipant;
        Assertions.assertThrows(IllegalArgumentException.class, () -> savedParticipant.setPrenom(null), "Une IllegalArgumentException devrait être déclenchée car le prénom est vide.");
        Participant p = participantService.update(id, updatedParticipant);
        Assertions.assertEquals(id, p.getParticipantId(), "L'objet modifié n'a pas le bon identifiant");
        Assertions.assertEquals(PRENOM, p.getPrenom(), "L'objet n'aurait pas dû être modifié.");
    }

    @Test
    void GivenParticipant_WhenUpdatedWithWrongId_ThenParticipantIsNull(){
        Long id = 1000L;
        Participant updatedParticipant = savedParticipant;
        updatedParticipant.setNom("TEST");
        Participant p = participantService.update(id, updatedParticipant);
        Assertions.assertNull(p, "Le participant n'existe pas; doit être null");
    }

    @Test
    void GivenParticipant_WhenDelete_ThenCorrectlyDeleted(){
        Long id = savedParticipant.getParticipantId();
        int result = participantService.delete(id);
        Assertions.assertEquals(1, result, "Le participant n'existait déjà plus");
    }

    @Test
    void WhenDeleteByWrongId_ThenError(){
        Long id = 1000L;
        int result = participantService.delete(id);
        Assertions.assertEquals(0, result, "Le participant n'existait déjà plus");
    }
}
