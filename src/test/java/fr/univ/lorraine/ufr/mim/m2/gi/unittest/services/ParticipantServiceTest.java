package fr.univ.lorraine.ufr.mim.m2.gi.unittest.services;

import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.exceptions.BadRequestException;
import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.exceptions.NotFoundException;
import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.models.Participant;
import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.repositories.ParticipantRepository;
import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.services.ParticipantService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class ParticipantServiceTest {

    @Mock
    private ParticipantRepository participantRepository;
    @InjectMocks
    private ParticipantService participantService;
    private Participant mockedParticipant;


    @BeforeEach
    void setUp() {
        mockedParticipant = mock(Participant.class);
    }

    @Test
    void givenParticipant_whenCreate_ThenParticipantRepositoryIsCalled() {
        try {
            // Given participant,
            // When
            participantService.create(mockedParticipant);
            // Then
            verify(participantRepository, times(1)).save(same(mockedParticipant));
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Test
    void whenGetAll_thenFindAllIsCalled() {
        participantService.getAll();
        // Then
        verify(participantRepository, times(1)).findAll();
    }

    @Test
    void whenFindById_thenMethodIsCalledOnce() {
        Long id = 1L;

        // Mocking repository response
        when(participantRepository.findById(id)).thenReturn(Optional.of(mockedParticipant));

        //When
        try {
            participantService.getById(id);
            // Then
            verify(participantRepository, times(1)).getById(same(id));
        } catch (BadRequestException | NotFoundException ignored) {

        }



    }

    @Test
    void givenASondageIdAndAnUpdatedSondage_whenUpdateSondage_thenSondageRepositoryGetByIdAndSaveIsCalled() {
        // Given
        Long id = 1L;

        // Mocking repository response
        when(participantRepository.findById(id)).thenReturn(Optional.of(mockedParticipant));

        // When
        participantService.update(id, mockedParticipant);

        // Then
        verify(participantRepository, times(1)).findById(same(id));
        verify(participantRepository, times(1)).save(same(mockedParticipant));
    }

    @Test
    void givenAnExistingSondageId_whenDeleteSondage_thenSondageRepositoryDeleteIsCalled() {
        // Given
        Long existingSondageId = 1L;

        // Mocking repository response
        when(participantRepository.findById(existingSondageId)).thenReturn(Optional.of(mockedParticipant));

        // When
        participantService.delete(existingSondageId);

        // Then
        verify(participantRepository, times(1)).findById(same(existingSondageId));
        verify(participantRepository, times(1)).deleteById(same(existingSondageId));
    }



}