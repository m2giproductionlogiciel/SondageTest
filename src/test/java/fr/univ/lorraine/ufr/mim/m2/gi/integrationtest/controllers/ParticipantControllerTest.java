package fr.univ.lorraine.ufr.mim.m2.gi.integrationtest.controllers;

import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.MySurveyApplication;
import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.models.Participant;
import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.services.ParticipantService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

import java.text.MessageFormat;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(classes = MySurveyApplication.class)
@AutoConfigureMockMvc
@TestPropertySource(locations = "classpath:application-integrationtest.properties")
@Transactional
class ParticipantControllerTest {
    @Autowired
    private ParticipantService participantService;
    @Autowired
    private MockMvc mockMvc;
    private final String NOM = "NOM";
    private final String PRENOM = "PRENOM";
    private Long alreadyExistingID = 1L;
    private final Long toBeCreatedID = 2000000L;

    @BeforeEach
    public void setUp(){
        Participant participant = new Participant();
        participant.setParticipantId(alreadyExistingID);
        participant.setNom(NOM);
        participant.setPrenom(PRENOM);
        participant = participantService.create(participant);
        alreadyExistingID = participant.getParticipantId();
    }

    @Test
    void WhenGetAllParticipantsThenGetAll() throws Exception {
        mockMvc.perform(get("/api/participant/"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").exists())
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$[0].nom").value(NOM))
                .andExpect(jsonPath("$[0].prenom").value(PRENOM))
        ;
    }

    @Test
    void WhenUseIdToGetParticipantThenParticipantFound() throws Exception {
        mockMvc.perform(get("/api/participant/{id}", alreadyExistingID))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.nom").value(NOM))
                .andExpect(jsonPath("$.prenom").value(PRENOM))
                .andExpect(jsonPath("$.participantId").value(alreadyExistingID))
        ;
    }

    @Test
    void WhenUseUnknownIdThenGetError404() throws Exception {
        mockMvc.perform(get("/api/participant/{id}", 2000000L))
                .andExpect(status().isNotFound())
                .andExpect(jsonPath("$.message").value("Participant introuvable."));
    }

    @Test
    void WhenPostEmptyObjectThenErrorBadRequest() throws Exception
    {
        mockMvc.perform(post("/api/participant/")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{}"))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.message").value("Requête malformée. Veuillez fournir les champs nom et prenom avec des valeurs non nuls."));
    }

    @Test
    void WhenPostParticipantWithExistingIdThenErrorBadRequest() throws Exception
    {
        mockMvc.perform(post("/api/participant/")

                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\n" +
                                "    \"nom\": \""+NOM+"\",\n" +
                                "    \"prenon\": \""+PRENOM+"\"\n" +
                                "}"))
                .andExpect(status().isBadRequest())
                .andExpect(result -> System.out.println(result.getResponse().getContentAsString()))
                .andExpect(jsonPath("$.message").value("Requête malformée. Veuillez fournir les champs nom et prenom avec des valeurs non nuls."));
    }

    @Test
    void GivenExistingParticipantWhenUpdateThenCorrectlyUpdated() throws Exception
    {
        String updatedNom = "nouveau nom";
        String updatePrenom = "nouveau prénom";

        mockMvc.perform(put("/api/participant/{id}", alreadyExistingID)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{" +
                                "\"nom\": \"" + updatedNom + "\",\n" +
                                "\"prenom\": \"" + updatePrenom + "\"\n" +
                                "}")
                )
                .andExpect(jsonPath("$.nom").value(updatedNom))
                .andExpect(jsonPath("$.prenom").value(updatePrenom))
                .andExpect(status().isOk());
    }

    @Test
    void GivenUnknownParticipantWhenUpdateThenNotFound() throws Exception
    {
        String updatedNom = "nouveau nom";
        String updatePrenom = "nouveau prénom";

        mockMvc.perform(put("/api/participant/{id}", toBeCreatedID)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{" +
                                "\"nom\": \"" + updatedNom + "\",\n" +
                                "\"prenom\": \"" + updatePrenom + "\"\n" +
                                "}")
                )
                .andExpect(status().isNotFound());
    }

    @Test
    void GivenEmptyNomWhenUpdateThenBadRequest() throws Exception
    {
        String updatePrenom = "nouveau prénom";

        mockMvc.perform(put("/api/participant/{id}", alreadyExistingID)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(MessageFormat.format("'{'\"nom\": {0},\n\"prenom\": \"{1}\"\n'}'", null, updatePrenom))
                )
                .andExpect(jsonPath("$.message").value("Requête malformée. Veuillez fournir les champs nom et prenom avec des valeurs non nuls."))
                .andExpect(status().isBadRequest());
    }

    @Test
    void GivenEmptyValuesWhenUpdateThenBadRequest() throws Exception
    {
        mockMvc.perform(put("/api/participant/{id}", alreadyExistingID)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\n" +
                                "\"nom\": \"\",\n" +
                                "\"prenom\": \"\"\n" +
                                "}")
                )
                .andExpect(jsonPath("$.message").value("Requête malformée. Veuillez fournir les champs nom et prenom avec des valeurs non nuls."))
                .andExpect(status().isBadRequest());
    }

    @Test
    void GivenEmptyPrenomWhenUpdateThenBadRequest() throws Exception
    {
        String updatedNom = "test";
        String updatePrenom = null;

        mockMvc.perform(put("/api/participant/{id}", alreadyExistingID)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(MessageFormat.format("'{'\"nom\": \"{0}\",\n\"prenom\": {1}\n'}'", updatedNom, updatePrenom))
                )
                .andExpect(jsonPath("$.message").value("Requête malformée. Veuillez fournir les champs nom et prenom avec des valeurs non nuls."))
                .andExpect(status().isBadRequest());
    }

    @Test
    void GivenMissingKeyWhenUpdateThenBadRequest() throws Exception
    {
        String updatePrenom = "Nouveau prénom";

        mockMvc.perform(put("/api/participant/{id}", alreadyExistingID)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{" +
                                "\"prenom\": \"" + updatePrenom + "\"\n" +
                                "}")
                )
                .andExpect(jsonPath("$.message").value("Requête malformée. Veuillez fournir les champs nom et prenom avec des valeurs non nuls."))
                .andExpect(status().isBadRequest());
    }

    @Test
    void GivenExistingParticipantWhenDeleteParticipantThenDeleted() throws Exception {
        mockMvc.perform(delete("/api/participant/{id}", alreadyExistingID))
                .andExpect(status().isOk());
    }


    @Test
    void GivenUnknownParticipantWhenDeleteThenError() throws Exception {
        mockMvc.perform(delete("/api/participant/{id}", toBeCreatedID))
                .andExpect(status().isOk());
    }
}
