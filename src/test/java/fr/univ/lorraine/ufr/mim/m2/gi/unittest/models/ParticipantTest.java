package fr.univ.lorraine.ufr.mim.m2.gi.unittest.models;

import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.models.Commentaire;
import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.models.DateSondee;
import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.models.Participant;
import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.models.Sondage;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.LinkedList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class ParticipantTest {
    private Participant nullParticipant;
    private Participant participant;


    @BeforeEach
    void setUp() {
        nullParticipant = new Participant();
        participant = new Participant(1L, "nom", "prenom");
    }


    @Test
    void givenUnsavedParticipant_whenGetId_thenParticipantIdIsNull() {
        assertNull(nullParticipant.getParticipantId(), "Le participant non enregristré doit avoir un id null");
    }

    @Test
    void givenParticipant_whenGetId_thenParticipantIdIsNotNull() {
        assertEquals(1L, participant.getParticipantId(), "Le participant 'enregristré' doit avoir un id non-null");
    }


    @Test
    void givenParticipant_whenSetNom_thenNomIsSet() {
        String nom = "test";
        participant.setNom(nom);
        assertEquals(nom, participant.getNom(), "Le nom du participant n'est pas correct après modification");
    }

    @Test
    void givenParticipant_whenSetPrenom_thenPrenomIsSet() {
        String prenom = "test";
        participant.setPrenom(prenom);
        assertEquals(prenom, participant.getPrenom(), "Le prénom du participant n'est pas correct après modification");
    }

    @Test
    void givenParticipant_whenNullSetNom_thenExceptionIsThrown() {
        String nom = null;
        assertThrows(IllegalArgumentException.class, () -> participant.setNom(nom), "Le nom du participant ne doit pas être null");
    }

    @Test
    void givenParticipant_whenNullSetPrenom_thenExceptionIsThrown() {
        String prenom = null;
        assertThrows(IllegalArgumentException.class, () -> participant.setPrenom(prenom), "Le prénom du participant ne doit pas être null");
    }

    @Test
    void givenParticipant_whenEmptySetNom_thenExceptionIsThrown() {
        String nom = "";

        assertThrows(IllegalArgumentException.class, () -> participant.setNom(nom), "Le nom du participant ne doit pas être null");
    }

    @Test
    void givenParticipant_whenEmptySetPrenom_thenExceptionIsThrown() {
        String prenom = "";
        assertThrows(IllegalArgumentException.class, () ->  participant.setPrenom(prenom), "Le prénom du participant ne doit pas être null");
    }

    @Test
    void givenParticipant_WhenSetCommentaires_thenGetSameCommentaireList() {
        List<Commentaire> commentaires = new LinkedList<>();
        participant.setCommentaire(commentaires);
        assertEquals(commentaires, participant.getCommentaire(), "La liste des commentaires n'est pas ajouté correctement à un participant.");
    }


    @Test
    void givenParticipant_WhenSetSondages_thenGetSameSondageList() {
        List<Sondage> sondages = new LinkedList<>();
        participant.setSondages(sondages);
        assertEquals(sondages, participant.getSondages(), "La liste des sondage n'est pas ajouté correctement à un participant.");
    }

    @Test
    void givenParticipant_WhenSetDateSondees_thenGetSameDateSondeeList() {
        List<DateSondee> dateSondees = new LinkedList<>();
        participant.setDateSondee(dateSondees);
        assertEquals(dateSondees, participant.getDateSondee(), "La liste des dates sondées n'est pas ajouté correctement à un participant.");
    }

    @Test
    void givenIdenticalParticipants_WhenTestEquality_thenIsTrue() {
        assertEquals(participant, new Participant(1L, "nom", "prenom"), "Les participants devraient être considérés comme identiques.");
    }

    @Test
    void givenDifferentParticipants_WhenTestEquality_thenIsFalse() {
        assertNotEquals(participant, new Participant(2L, "test", "test"), "Les participants devraient être considérés comme différents.");
    }

    @Test
    void givenDifferentParticipantsByNameAndSurname_WhenTestEquality_thenIsFalse() {
        assertNotEquals(participant, new Participant(1L, "test", "test"), "Les participants devraient être considérés comme différents.");
    }

    @Test
    void givenDifferentParticipantsBySurname_WhenTestEquality_thenIsFalse() {
        assertNotEquals(participant, new Participant(1L, "nom", "test"), "Les participants devraient être considérés comme différents.");
    }

    @Test
    void givenDifferentParticipantsByName_WhenTestEquality_thenIsFalse() {
        assertNotEquals(participant, new Participant(1L, "test", "prenom"), "Les participants devraient être considérés comme différents.");
    }
    @Test
    void givenParticipant_WhenGetCommentaires_thenEmptyList() {
        List<Commentaire> commentaires = participant.getCommentaire();
        assertEquals(0, commentaires.size(), "Le participant n'a pas de commentaires et n'est pas null.");
    }

    @Test
    void givenParticipant_WhenGetSondages_thenEmptyList() {
        List<Sondage> sondages = participant.getSondages();
        assertEquals(0, sondages.size(), "Le participant n'a pas de sondages et n'est pas null.");
    }

    @Test
    void givenParticipant_WhenGetDateSondees_thenEmptyList() {
        List<DateSondee> dateSondees = participant.getDateSondee();
        assertEquals(0, dateSondees.size(), "Le participant n'a pas de dateSondees et n'est pas null.");
    }

    @Test
    void givenParticipant_WhenCompareSameValuesInObject_ThenObjectsAreEquals(){
        Participant p = new Participant(1L, "nom", "prenom");
        assertEquals(participant.hashCode(), p.hashCode(), "Le participant devrait avoir le même hashcode");
        assertEquals(participant, p, "Le participant devrait être le même");
    }

    @Test
    void givenParticipant_WhenCompareObjectToNullObject_ThenObjectsAreNotEquals(){
        assertNotEquals(participant.hashCode(), nullParticipant.hashCode(), "Le participant devrait avoir le même hashcode");
        assertNotEquals(participant, nullParticipant, "Les participants ne devraient pas être les mêmes.");
    }

    @Test
    void givenParticipant_WhenCompareToObjectWithId_ThenObjectsAreNotEquals(){
        Participant p = new Participant(1000L, "nom", "prenom");
        assertNotEquals(participant.hashCode(), p.hashCode(), "Les participants ne devraient pas être les mêmes.");
        assertNotEquals(participant, p, "Les participants ne devraient pas être les mêmes.");
    }
    @Test
    void givenParticipant_WhenCompareToObjectWithDifferentName_ThenObjectsAreNotEquals(){
        Participant p = new Participant(1L, "test", "prenom");
        assertNotEquals(participant.hashCode(), p.hashCode(), "Les participants ne devraient pas être les mêmes.");
        assertNotEquals(participant, p, "Les participants ne devraient pas être les mêmes.");
    }

    @Test
    void givenParticipant_WhenCompareToObjectWithDifferentSurname_ThenObjectsAreNotEquals(){
        Participant p = new Participant(1L, "nom", "test");
        assertNotEquals(participant.hashCode(), p.hashCode(), "Les participants ne devraient pas être les mêmes.");
        assertNotEquals(participant, p, "Les participants ne devraient pas être les mêmes.");
    }

}