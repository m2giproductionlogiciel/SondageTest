package fr.univ.lorraine.ufr.mim.m2.gi.unittest.models;

import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.models.DateSondage;
import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.models.DateSondee;
import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.models.Sondage;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;



class DateSondageTest {

    private DateSondage dateSondage;

    // Configuration initiale pour chaque test
    @BeforeEach
    public void setUp() {
        dateSondage = new DateSondage();
    }

    // Teste le getter et le setter pour dateSondageId
    @Test
    void getterAndSetterDateSondageId() {
        Long dateSondageId = 1L;
        dateSondage.setDateSondageId(dateSondageId);
        assertEquals(dateSondageId, dateSondage.getDateSondageId());
    }

    // Teste le getter et le setter pour la date
    @Test
    void getterAndSetterDate() {
        Date date = new Date();
        dateSondage.setDate(date);
        assertEquals(date, dateSondage.getDate());
    }

    // Teste le getter et le setter pour l'objet Sondage
    @Test
    void getterAndSetterSondage() {
        Sondage sondage = new Sondage();
        dateSondage.setSondage(sondage);
        assertEquals(sondage, dateSondage.getSondage());
    }

    // Teste le getter et le setter pour la liste de DateSondee
    @Test
    void getterAndSetterDateSondee() {
        List<DateSondee> dateSondeeList = new ArrayList<>();
        dateSondage.setDateSondee(dateSondeeList);
        assertEquals(dateSondeeList, dateSondage.getDateSondee());
    }

    // Teste le constructeur avec des paramètres
    @Test
    void testConstructorWithParameters() {
        // Initialisation des valeurs de test
        Long dateSondageId = 1L;
        Date date = new Date();
        Sondage sondage = new Sondage();
        List<DateSondee> dateSondeeList = new ArrayList<>();

        // Appel du constructeur avec les paramètres
        DateSondage newDateSondage = new DateSondage(dateSondageId, date, sondage, dateSondeeList);

        // Assertions pour vérifier que les valeurs ont été correctement attribuées
        assertEquals(dateSondageId, newDateSondage.getDateSondageId());
        assertEquals(date, newDateSondage.getDate());
        assertEquals(sondage, newDateSondage.getSondage());
        assertEquals(dateSondeeList, newDateSondage.getDateSondee());
    }

    // Teste le constructeur par défaut, sans paramètre
    @Test
    void testDefaultConstructor() {
        assertNotNull(dateSondage);
    }

    // Teste la récupération du sondage
    @Test
    void getSondage() {
        Sondage expectedSondage = new Sondage();
//        DateSondage dateSondage = new DateSondage();
        dateSondage.setSondage(expectedSondage);

        Sondage actualSondage = dateSondage.getSondage();

        // vérifier que la récupération du Sondage à partir de DateSondage fonctionne correctement

        assertEquals(expectedSondage, actualSondage);
    }

    // Teste la relation ManyToOne avec Sondage
    @Test
    void relationManyToOneSondage() {
        // Créer un sondage fictif
        Sondage sondage = new Sondage();
        sondage.setSondageId(1L);
        sondage.setNom("Sondage de test");

        // Créer une date de sondage en utilisant la relation ManyToOne
        DateSondage dateSondage = new DateSondage();
        dateSondage.setSondage(sondage);

        // Vérifier que la relation a été correctement établie
        assertEquals(sondage, dateSondage.getSondage());
    }

}