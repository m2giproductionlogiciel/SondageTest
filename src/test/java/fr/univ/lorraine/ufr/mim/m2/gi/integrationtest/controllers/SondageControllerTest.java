package fr.univ.lorraine.ufr.mim.m2.gi.integrationtest.controllers;

import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.MySurveyApplication;
import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.exceptions.BadRequestException;
import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.exceptions.NotFoundException;
import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.models.Choix;
import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.models.Participant;
import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.models.Sondage;
import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.services.ParticipantService;
import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.services.SondageService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.util.Date;
import java.util.TimeZone;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(classes = MySurveyApplication.class)
@AutoConfigureMockMvc
@TestPropertySource(locations = "classpath:application-integrationtest.properties")
@Transactional
public class SondageControllerTest {

    @Autowired
    private ParticipantService participantService;
    @Autowired
    private SondageService sondageService;
    @Autowired
    private MockMvc mockMvc;
    private final String NOM = "NOM";
    private final String PRENOM = "PRENOM";
    private final String DESCRIPTION = "DESCRIPTION";
    private final boolean CLOTURE = false;
    private final Date DATE = java.sql.Date.valueOf(LocalDate.now());
    private Long participantId;
    private Long participant2Id;
    private Long sondageId;
    private Long sondage2Id;
    private String formattedDate;

    @BeforeEach
    public void setUp() throws BadRequestException, NotFoundException {

        /// -- Gestion de de la date
        // ------------------------
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSX");
        dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        formattedDate = dateFormat.format(new Date(DATE.getTime()));


        // -- Création et insertion du Participant n°1
        // -------------------------------------------
        Participant participant = new Participant();
        participant.setNom(NOM);
        participant.setPrenom(PRENOM);
        participant = participantService.create(participant);
        participantId = participant.getParticipantId();

        // -- Création et insertion du Participant n°2
        // -------------------------------------------
        Participant participant2 = new Participant();
        participant2.setNom(NOM);
        participant2.setPrenom(PRENOM);
        participant2 = participantService.create(participant2);
        participant2Id = participant2.getParticipantId();

        // -- Création et insertion du Sondage n°1
        // ---------------------------------------
        Sondage sondage = new Sondage();
        sondage.setNom(NOM);
        sondage.setDescription(DESCRIPTION);
        sondage.setCloture(CLOTURE);
        sondage.setFin(DATE);

        sondageService.create(participant.getParticipantId(), sondage);
        sondageId = sondage.getSondageId();


        // -- Création et insertion du Sondage n°2
        // ---------------------------------------
        Sondage sondage2 = new Sondage();
        sondage2.setNom(NOM);
        sondage2.setDescription(DESCRIPTION);
        sondage2.setCloture(CLOTURE);
        sondage2.setFin(DATE);

        sondageService.create(participant2.getParticipantId(), sondage2);
        sondage2Id = sondage2.getSondageId();
    }

    //region GetById
    @Test
    void GivenASondageId_whenGetSondageById_thenCheckSondage() throws Exception {

    mockMvc.perform(get("/api/sondage/{id}", sondageId))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.sondageId").value(sondageId))
                .andExpect(jsonPath("$.nom").value(NOM))
                .andExpect(jsonPath("$.description").value(DESCRIPTION))
                .andExpect(jsonPath("$.cloture").value(CLOTURE))
                .andExpect(jsonPath("$.createBy").value(participantId))
                .andExpect(jsonPath("$.fin").value(Date.from(Instant.ofEpochMilli(DATE.getTime()))));
    }
    @Test
    void GivenNotFoundSondageId_whenGetSondageById_thenNotFoundExceptionIsThrown() throws Exception {
        mockMvc.perform(get("/api/sondage/{id}", 99L))
                .andExpect(status().isNotFound());
    }

    @Test
    void GivenInvalidSondageId_whenGetSondageById_thenBadRequestExceptionIsThrown() throws Exception {
        mockMvc.perform(get("/api/sondage/{id}", 0L))
                .andExpect(status().isBadRequest());

    }
    //endregion

    //region GetAll
    @Test
    void Given2Sondage_whenGetAllSondage_thenCheckListSondage() throws Exception {
        mockMvc.perform(get("/api/sondage/"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").isArray())
                //.andExpect(jsonPath("$[0].sondageId").value(sondageId))
                .andExpect(jsonPath("$[0].nom").value(NOM))
                .andExpect(jsonPath("$[0].description").value(DESCRIPTION))
                .andExpect(jsonPath("$[0].fin").value(Date.from(Instant.ofEpochMilli(DATE.getTime()))))
                .andExpect(jsonPath("$[0].cloture").value(CLOTURE))
                //.andExpect(jsonPath("$[0].createBy").value(participantId))

                //.andExpect(jsonPath("$[1].sondageId").value(sondage2Id))
                .andExpect(jsonPath("$[1].nom").value(NOM))
                .andExpect(jsonPath("$[1].description").value(DESCRIPTION))
                .andExpect(jsonPath("$[1].fin").value(Date.from(Instant.ofEpochMilli(DATE.getTime()))))
                .andExpect(jsonPath("$[1].cloture").value(CLOTURE));
                //.andExpect(jsonPath("$[1].createBy").value(participant2Id));
    }
    //endregion

    //region Create
    @Test
    void GivenAParticipant_whenCreateSondage_thenCheckSondageIsValid() throws Exception {

        mockMvc.perform(post("/api/sondage/")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{"
                                + "\"sondageId\":3,"
                                + "\"nom\":\"" + NOM + "\","
                                + "\"description\":\"" + DESCRIPTION + "\","
                                + "\"fin\":\"" + formattedDate + "\","
                                + "\"cloture\":\"" + CLOTURE + "\","
                                + "\"createBy\":\"" + participantId + "\""
                                + "}"))



                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.nom").value(NOM))
                .andExpect(jsonPath("$.description").value(DESCRIPTION))
                .andExpect(jsonPath("$.fin").value(Date.from(Instant.ofEpochMilli(DATE.getTime()))))
                .andExpect(jsonPath("$.cloture").value(CLOTURE))
                .andExpect(jsonPath("$.createBy").value(participantId));
    }

    @Test
    void GivenInvalidParticipantId_whenCreateSondage_thenBadRequestExceptionIsThrown() throws Exception {
        mockMvc.perform(post("/api/sondage/")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{"
                                + "\"sondageId\":3,"
                                + "\"nom\":\"Nouveau Nom\","
                                + "\"description\":\"Nouvelle Description\","
                                + "\"fin\":\"2024-01-20\","
                                + "\"cloture\":true,"
                                + "\"createBy\":0"
                                + "}"))
                .andExpect(status().isBadRequest());
    }

    @Test
    void GivenNotFoundParticipantId_whenCreateSondage_thenNotFoundtExceptionIsThrown() throws Exception {
        mockMvc.perform(post("/api/sondage/")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{"
                                + "\"sondageId\":3,"
                                + "\"nom\":\"Nouveau Nom\","
                                + "\"description\":\"Nouvelle Description\","
                                + "\"fin\":\"2024-01-20\","
                                + "\"cloture\":true,"
                                + "\"createBy\":99"
                                + "}"))
                .andExpect(status().isNotFound());
    }
    //endregion

    //region Update
    @Test
    void GivenSondageId_whenUpdateSondage_thenCheckUpdatedSondage() throws Exception {
    mockMvc.perform(put("/api/sondage/{id}", sondageId)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{"
                                + "\"nom\":\"Nouveau Nom\","
                                + "\"description\":\"Nouvelle Description\","
                                + "\"fin\":\"" + formattedDate + "\","
                                + "\"cloture\":true,"
                                + "\"createBy\":\"" + participantId + "\""
                                + "}"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.nom").value("Nouveau Nom"))
                .andExpect(jsonPath("$.description").value("Nouvelle Description"))
                .andExpect(jsonPath("$.fin").value(Date.from(Instant.ofEpochMilli(DATE.getTime()))))
                .andExpect(jsonPath("$.cloture").value(true))
                .andExpect(jsonPath("$.createBy").value(participantId));
    }

    @Test
    void GivenNotFoundSondageId_whenUpdateSondage_thenNotFoundExceptionIsThrown() throws Exception {
        mockMvc.perform(put("/api/sondage/{id}", 99L)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{"
                                + "\"nom\":\"Nouveau Nom\","
                                + "\"description\":\"Nouvelle Description\","
                                + "\"fin\":\"2024-01-18\","
                                + "\"cloture\":true,"
                                + "\"createBy\":1"
                                + "}"))
                .andExpect(status().isNotFound());
    }

    @Test
    void GivenInvalidSondageId_whenUpdateSondage_thenBadRequestExceptionIsThrown() throws Exception {
        mockMvc.perform(put("/api/sondage/{id}", 0)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{"
                                + "\"nom\":\"Nouveau Nom\","
                                + "\"description\":\"Nouvelle Description\","
                                + "\"fin\":\"2024-01-18\","
                                + "\"cloture\":true,"
                                + "\"createBy\":1"
                                + "}"))
                .andExpect(status().isBadRequest());
    }
    //endregion

    //region Delete
    @Test
    void GivenSondageId_whenDeleteSondage_thenCheckIfSondageIsDeleted() throws Exception {
    mockMvc.perform(delete("/api/sondage/{id}", sondageId))
                .andExpect(status().isOk());
    }

    @Test
    void GivenNotFoundSondageId_whenDeleteSondage_thenNotFoundExceptionIsThrown() throws Exception {
        mockMvc.perform(get("/api/sondage/{id}", 99L))
                .andExpect(status().isNotFound());
    }

    @Test
    void GivenInvalidSondageId_whenDeleteSondage_thenBadRequestExceptionIsThrown() throws Exception {
        mockMvc.perform(get("/api/sondage/{id}", 0L))
                .andExpect(status().isBadRequest());
    }
    //endregion

}
