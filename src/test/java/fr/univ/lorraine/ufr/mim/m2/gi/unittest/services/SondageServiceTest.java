package fr.univ.lorraine.ufr.mim.m2.gi.unittest.services;

import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.exceptions.BadRequestException;
import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.exceptions.NotFoundException;
import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.models.Participant;
import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.models.Sondage;
import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.repositories.SondageRepository;
import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.services.ParticipantService;
import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.services.SondageService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class SondageServiceTest {

    @Mock
    private SondageRepository sondageRepository;

    @Mock
    private ParticipantService participantService;

    @InjectMocks
    private SondageService sondageService;

    //region Create
    @Test
    void givenASondageWithNonExistingParticipant_whenCreateSondage_thenNotFoundExceptionIsThrown() throws BadRequestException, NotFoundException {
        // Given
        Long participantId = 1L;
        Sondage sondage = mock(Sondage.class);

        // Mocking repository response to return null
        when(participantService.getById(participantId)).thenReturn(null);

        // When
        assertThrows(NotFoundException.class, () -> sondageService.create(participantId, sondage), "Expected NotFoundException to be thrown");

        // Then
        verify(sondageRepository, times(0)).save(any(Sondage.class));
    }

    @Test
    void givenASondage_whenCreateSondage_thenParticipantServiceAndSondageRepositoryIsCalled() throws BadRequestException, NotFoundException {
        // Given
        Long participantId = 1L;
        Sondage sondage = mock(Sondage.class);
        Participant participant = mock(Participant.class);

        // Mocking repository response
        when(participantService.getById(participantId)).thenReturn(participant);

        // When
        sondageService.create(participantId, sondage);

        // Then
        verify(participantService, times(1)).getById(participantId);
        verify(sondageRepository, times(1)).save(same(sondage));
    }

    @Test
    void givenASondageWithNullParticipant_whenCreateSondage_thenBadRequestExceptionIsThrown() {
        // Given
        Long participantId = null;
        Sondage sondage = mock(Sondage.class);

        // When
        assertThrows(BadRequestException.class, () -> sondageService.create(participantId, sondage), "Expected BadRequestException to be thrown");

        // Then
        verify(sondageRepository, times(0)).save(any(Sondage.class));
    }

    @Test
    void givenASondageWithParticipantIdAs0_whenCreateSondage_thenBadRequestExceptionIsThrown() {
        // Given
        Long participantId = 0L;
        Sondage sondage = mock(Sondage.class);

        // When
        assertThrows(BadRequestException.class, () -> sondageService.create(participantId, sondage), "Expected BadRequestException to be thrown");

        // Then
        verify(sondageRepository, times(0)).save(any(Sondage.class));
    }

    @Test
    void givenANullSondage_whenCreateSondage_thenBadRequestExceptionIsThrown() {
        // Given
        Long participantId = 1L;
        Sondage sondage = null;

        // When
        assertThrows(BadRequestException.class, () -> sondageService.create(participantId, sondage), "Expected BadRequestException to be thrown");

        // Then
        verify(sondageRepository, times(0)).save(any(Sondage.class));
    }

    //endregion

    //region GetById
    @Test
    void givenANonExistingSondage_whenGetSondageById_thenNotFoundExceptionIsThrown() {
        // Given
        Long sondageId = 1L;
        Sondage sondage = mock(Sondage.class);

        // Mocking repository response to return an empty Optional
        when(sondageRepository.findById(sondageId)).thenReturn(Optional.empty());

        // When
        assertThrows(NotFoundException.class, () -> sondageService.getById(sondageId), "Expected NotFoundException to be thrown");

        // Then
        verify(sondageRepository, times(0)).save(any(Sondage.class));
    }

    @Test
    void givenASondageId_whenGetSondageById_thenSondageRepositoryGetByIdIsCalled() throws BadRequestException, NotFoundException {
        // Given
        Long sondageId = 1L;
        Sondage sondage = mock(Sondage.class);

        // Mocking repository response
        when(sondageRepository.findById(sondageId)).thenReturn(Optional.of(sondage));

        // When
        sondageService.getById(sondageId);

        // Then
        verify(sondageRepository, times(1)).getById(same(sondageId));
    }

    @Test
    void givenSondageIdAsNull_whenGetSondageById_thenBadRequestExceptionIsThrown() {
        // Given
        Long sondageId = null;

        // When
        assertThrows(BadRequestException.class, () -> sondageService.getById(sondageId), "Expected BadRequestException to be thrown");

        // Then
        verify(sondageRepository, times(0)).getById(same(sondageId));
    }

    @Test
    void givenSondageIdAs0_whenGetSondageById_thenBadRequestExceptionIsThrown() {
        // Given
        Long sondageId = 0L;

        // When
        assertThrows(BadRequestException.class, () -> sondageService.getById(sondageId), "Expected BadRequestException to be thrown");

        // Then
        verify(sondageRepository, times(0)).getById(same(sondageId));
    }

    //endregion

    //region FindAll
    @Test
    void whenGetAllSondage_thenSondageRepositoryFindAllIsCalled() {
        // Given

        // When
        sondageService.getAll();

        // Then
        verify(sondageRepository, times(1)).findAll();
    }
    //endregion

    //region Update
    @Test
    void givenANonExistingSondageIdAndAnUpdatedSondage_whenUpdateSondage_thenNotFoundExceptionIsThrown() {
        // Given
        Long existingSondageId = 1L;
        Sondage updatedSondage = mock(Sondage.class);

        // Mocking repository response
        when(sondageRepository.findById(existingSondageId)).thenReturn(Optional.empty());

        // When
        assertThrows(NotFoundException.class, () -> sondageService.update(existingSondageId, updatedSondage), "Expected NotFoundException to be thrown");

        // Then
        verify(sondageRepository, times(0)).save(any(Sondage.class));
    }

    @Test
    void givenASondageIdAndAnUpdatedSondage_whenUpdateSondage_thenSondageRepositoryGetByIdAndSaveIsCalled() throws BadRequestException, NotFoundException {
        // Given
        Long existingSondageId = 1L;
        Sondage updatedSondage = mock(Sondage.class);

        // Mocking repository response
        when(sondageRepository.findById(existingSondageId)).thenReturn(Optional.of(updatedSondage));

        // When
        sondageService.update(existingSondageId, updatedSondage);

        // Then
        verify(sondageRepository, times(1)).findById(same(existingSondageId));
        verify(sondageRepository, times(1)).save(same(updatedSondage));
    }

    @Test
    void givenASondageIdAsNullAndAnUpdatedSondage_whenUpdateSondage_thenBadRequestExceptionIsThrown() {
        // Given
        Long existingSondageId = null;
        Sondage updatedSondage = mock(Sondage.class);

        // When
        assertThrows(BadRequestException.class, () -> sondageService.update(existingSondageId, updatedSondage), "Expected BadRequestException to be thrown");

        // Then
        verify(sondageRepository, times(0)).save(any(Sondage.class));
    }

    @Test
    void givenASondageIdAs0AndAnUpdatedSondage_whenUpdateSondage_thenBadRequestIsThrown() {
        // Given
        Long existingSondageId = 0L;
        Sondage updatedSondage = mock(Sondage.class);

        // When
        assertThrows(BadRequestException.class, () -> sondageService.update(existingSondageId, updatedSondage), "Expected BadRequestException to be thrown");

        // Then
        verify(sondageRepository, times(0)).save(any(Sondage.class));
    }
    //endregion

    //region Delete
    @Test
    void givenAnExistingSondageId_whenDeleteSondage_thenSondageRepositoryDeleteIsCalled() throws BadRequestException, NotFoundException {
        // Given
        Long existingSondageId = 1L;
        Sondage existingSondage = mock(Sondage.class);

        // Mocking repository response
        when(sondageRepository.findById(existingSondageId)).thenReturn(Optional.of(existingSondage));

        // When
        sondageService.delete(existingSondageId);

        // Then
        verify(sondageRepository, times(1)).findById(same(existingSondageId));
        verify(sondageRepository, times(1)).deleteById(same(existingSondageId));
    }

    @Test
    void givenASondageIdAsNull_whenDeleteSondage_thenBadRequestExceptionIsThrown() {
        // Given
        Long existingSondageId = null;

        // When
        assertThrows(BadRequestException.class, () -> sondageService.delete(existingSondageId), "Expected BadRequestException to be thrown");

        // Then
        verify(sondageRepository, times(0)).save(any(Sondage.class));
    }

    @Test
    void givenASondageIdAs0_whenDeleteSondage_thenBadRequestExceptionIsThrown() {
        // Given
        Long existingSondageId = 0L;

        // When
        assertThrows(BadRequestException.class, () -> sondageService.delete(existingSondageId), "Expected BadRequestException to be thrown");

        // Then
        verify(sondageRepository, times(0)).save(any(Sondage.class));
    }

    @Test
    void givenANonExistingSondageId_whenDeleteSondage_thenNotFoundExceptionIsThrown() {
        // Given
        Long existingSondageId = 1L;

        // Mocking repository response
        when(sondageRepository.findById(existingSondageId)).thenReturn(Optional.empty());

        // When
        assertThrows(NotFoundException.class, () -> sondageService.delete(existingSondageId), "Expected NotFoundException to be thrown");

        // Then
        verify(sondageRepository, times(0)).save(any(Sondage.class));
    }

    //endregion
}
