package fr.univ.lorraine.ufr.mim.m2.gi.unittest.models;

import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.models.Sondage;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.transaction.annotation.Transactional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

@Transactional
class SondageTest {

    private Sondage sondage;

    @BeforeEach
    public void setUp(){
        sondage = new Sondage();
    }


    //region SetSondageId
    @Test
    void givenSondage_whenSetId_thenSondageIdIsSet() {
        sondage.setSondageId(1L);
        assertEquals(1L, sondage.getSondageId(), "Le sondage doit avoir un id egale à 1.");
    }
    //endregion

    //region GetSondageId
    @Test
    void givenUnsavedSondage_whenGetId_thenSondageIdIsNull() {
        assertNull(sondage.getSondageId(), "L'Id du sondage non enregristré doit être null.");
    }
    //endregion

    //region SetNom
    @Test
    void givenSondage_whenSetNom_thenNomIsSet() {
        String nom = "test";
        sondage.setNom(nom);
        assertEquals(nom, sondage.getNom(), "Le nom du sondage n'est pas correct après modification");
    }

    //endregion

    //region SetDescription
    @Test
    void givenSondage_whenSetDescription_thenDescriptionIsSet() {
        String description = "test";
        sondage.setDescription(description);
        assertEquals(description, sondage.getDescription(), "La description du sondage n'est pas correct après modification");
    }
    //endregion

    //region SetCloture
    @Test
    void givenSondage_whenSetCloture_thenClotureIsSet() {
        Boolean cloture = false;
        sondage.setCloture(cloture);
        assertEquals(cloture, sondage.getCloture(), "La cloture du sondage n'est pas correct après modification");
    }
    //endregion

}
