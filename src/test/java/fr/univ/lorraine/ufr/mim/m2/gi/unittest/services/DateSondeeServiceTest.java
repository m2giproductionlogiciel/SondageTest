package fr.univ.lorraine.ufr.mim.m2.gi.unittest.services;

import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.exceptions.BadRequestException;
import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.exceptions.NotFoundException;
import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.models.DateSondage;
import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.models.DateSondee;
import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.models.Sondage;
import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.repositories.DateSondeeRepository;
import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.services.DateSondageService;
import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.services.DateSondeeService;
import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.services.ParticipantService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.mockito.ArgumentMatchers.same;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class DateSondeeServiceTest {

    @Mock
    private DateSondeeRepository dateSondeeRepository;

    @Mock
    private DateSondageService dateSondageService;

    @Mock
    private ParticipantService participantService;
    @InjectMocks
    private DateSondeeService dateSondeeService;

    private DateSondee mockedDateSondee;

    @BeforeEach
    void setUp() {
        mockedDateSondee = mock(DateSondee.class);
        //mockedDateSondee.getDateSondage().setDate(new Date());
    }

    @Test
    void WhenCreateThenSaved() throws BadRequestException, NotFoundException {
        List<DateSondee> list = new ArrayList<>();
        list.add(mockedDateSondee);

        doReturn(new DateSondage(1L, new Date(), mock(Sondage.class), list)).when(dateSondageService).getById(anyLong());

        dateSondeeService.create(1L, 1L, mockedDateSondee);
        // Then
        verify(dateSondeeRepository, times(1)).save(same(mockedDateSondee));
    }

    @Test
    void WhenBestDateIsCalledThenRepoCallBestDate(){
        dateSondeeService.bestDate(1L);
        verify(dateSondeeRepository, times(1)).bestDate(anyLong());
    }

    @Test
    void WhenMaybeBestDateIsCalledThenRepoCallMaybeBestDate(){
        dateSondeeService.maybeBestDate(1L);
        verify(dateSondeeRepository, times(1)).maybeBestDate(anyLong());
    }
}
