package fr.univ.lorraine.ufr.mim.m2.gi.unittest.models;

import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.models.Commentaire;
import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.models.Participant;
import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.models.Sondage;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CommentaireTest {

    private Commentaire commentaire;

    @BeforeEach
    void setUp() {
        commentaire = new Commentaire();
    }

    @Test
    void givenCommentaireIdIsSet_whenGetId_thenIdIsSet() {
        commentaire.setCommentaireId(1L);
        assertEquals(1L, commentaire.getCommentaireId(), "L'ID du commentaire n'est pas défini avec le setter");
    }

    @Test
    void givenCommentaireIsCreated_whenGetId_thenIdIsNull() {
        assertNull(commentaire.getCommentaireId(), "L'ID du commentaire devrait être nul lors de la création");
    }

    @Test
    void givenCommentaireIsCreated_whenGetCommentaire_thenCommentaireIsNull() {
        assertNull(commentaire.getCommentaire(), "Le commentaire devrait être nul lors de la création");
    }

    @Test
    void givenCommentaire_whenSetId_thenIdCannotBeNull() {
        assertThrows(IllegalArgumentException.class, () -> {
            commentaire.setCommentaireId(null);
        }, "L'ID du commentaire sauvegardé ne peut pas être nul");
    }

    @Test
    void givenCommentaire_whenSetId_thenIdCannotBeNegative() {
        assertThrows(IllegalArgumentException.class, () -> {
            commentaire.setCommentaireId(-1L);
        }, "L'ID du commentaire sauvegardé ne peut pas être négatif");
    }

    @Test
    void givenCommentaireIsSet_whenGetCommentaire_thenCommentaireIsSet() {
        commentaire.setCommentaire("Ceci est un commentaire");
        assertEquals("Ceci est un commentaire", commentaire.getCommentaire(), "Le commentaire n'est pas défini avec le setter");
    }

    @Test
    void givenCommentaire_whenSetCommentaire_thenCommentaireCannotBeNull() {
        assertThrows(IllegalArgumentException.class, () -> {
            commentaire.setCommentaire(null);
        }, "Le commentaire sauvegardé ne peut pas être nul");
    }

    @Test
    void givenCommentaire_whenSetId_thenCommentaireCannotBeEmpty() {
        assertThrows(IllegalArgumentException.class, () -> {
            commentaire.setCommentaire("");
        }, "Le commentaire sauvegardé ne peut pas être vide");
    }

    @Test
    void givenSondageIsSet_whenGetSondage_thenSondageIsSet() {
        Sondage sondage = new Sondage();
        commentaire.setSondage(sondage);
        assertEquals(sondage, commentaire.getSondage(), "Le sondage n'est pas défini avec le setter");
    }

    @Test
    void givenSondageIsSet_whenGetSondage_thenSondageCannotBeNull() {
        assertThrows(IllegalArgumentException.class, () -> {
            commentaire.setSondage(null);
        }, "Le sondage ne peut pas être nul");
    }

    @Test
    void givenParticipantIsSet_whenGetParticipant_thenParticipantIsSet() {
        Participant participant = new Participant();
        commentaire.setParticipant(participant);
        assertEquals(participant, commentaire.getParticipant(), "Le participant n'est pas défini avec le setter");
    }

    @Test
    void givenParticipantIsSet_whenGetParticipant_thenParticipantCannotBeNull() {
        assertThrows(IllegalArgumentException.class, () -> {
            commentaire.setParticipant(null);
        }, "Le participant ne peut pas être nul");
    }
}
