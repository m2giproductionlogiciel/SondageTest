package fr.univ.lorraine.ufr.mim.m2.gi.integrationtest.services;

import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.controllers.DateSondageController;
import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.dtos.DateSondeeDto;
import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.exceptions.BadRequestException;
import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.exceptions.NotFoundException;
import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.models.DateSondage;
import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.models.DateSondee;
import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.services.DateSondageService;
import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.services.DateSondeeService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.modelmapper.ModelMapper;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class DateSondageIntegrationTest {

    @Mock
    private DateSondageService dateSondageService;

    @Mock
    private DateSondeeService dateSondeeService;

    @Mock
    private ModelMapper modelMapper;

    @InjectMocks
    private DateSondageController dateSondageController;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void delete() throws NotFoundException {
        // Test de la méthode delete du controller
        dateSondageController.delete(1L);

        // Vérification que la méthode correspondante du service a été appelée
        verify(dateSondageService, times(1)).delete(1L);
    }

    @Test
    void givenValidDate_whenCreate_thenDateSondageReturned() throws BadRequestException, NotFoundException {
        // Given
        DateSondage dateSondage = new DateSondage();
        dateSondage.setDateSondageId(1L);
        dateSondage.setDate(new Date());

        // Mock the behavior of the dateSondageService, not dateSondageRepository
        when(dateSondageService.create(anyLong(), any(DateSondage.class))).thenReturn(dateSondage);

        // When
        DateSondage result = dateSondageService.create(1L, dateSondage);

        // Then
        assertEquals(dateSondage, result);
        verify(dateSondageService, times(1)).create(anyLong(), any(DateSondage.class));
    }

    @Test
    void givenValidSondageId_whenGetBySondageId_thenListDateSondageReturned() {
        // Given
        DateSondage dateSondage1 = new DateSondage();
        dateSondage1.setDateSondageId(1L);
        dateSondage1.setDate(new Date());

        DateSondage dateSondage2 = new DateSondage();
        dateSondage2.setDateSondageId(2L);
        dateSondage2.setDate(new Date());

        List<DateSondage> dateSondageList = new ArrayList<>();
        dateSondageList.add(dateSondage1);
        dateSondageList.add(dateSondage2);

        // Mock the behavior of dateSondageService
        when(dateSondageService.getBySondageId(1L)).thenReturn(dateSondageList);

        // When
        List<DateSondage> result = dateSondageService.getBySondageId(1L);

        // Then
        assertEquals(dateSondageList, result);
    }


    @Test
    void createParticipation() throws BadRequestException, NotFoundException {
        // Créer des objets fictifs nécessaires pour le test
        DateSondeeDto dto = new DateSondeeDto();
        dto.setParticipant(1L);

        DateSondee dateSondee = new DateSondee();

        // Mock le comportement de modelMapper.map pour les arguments donnés
        when(modelMapper.map(any(DateSondeeDto.class), eq(DateSondee.class))).thenReturn(dateSondee);
        when(dateSondeeService.create(anyLong(), anyLong(), any(DateSondee.class))).thenReturn(dateSondee);
        when(modelMapper.map(dateSondee, DateSondeeDto.class)).thenReturn(dto);

        // Test de la méthode createParticipation du controller
        ResponseEntity<DateSondeeDto> result = dateSondageController.createParticipation(1L, dto);


        // Vérification que les méthodes correspondantes du service et du modelMapper ont été appelées
        verify(dateSondeeService, times(1)).create(1L, dto.getParticipant(), dateSondee);

        // Remplacer l'appel d'origine à modelMapper.map par any() pour la vérification
        verify(modelMapper, times(1)).map(any(DateSondeeDto.class), eq(DateSondee.class));
        verify(modelMapper, times(1)).map(dateSondee, DateSondeeDto.class);

        // Vérification du résultat
        assertNotNull(result);
    }

    @Test
    void createParticipationWhenCreateReturnsNull() throws BadRequestException, NotFoundException {
        // Créer des objets fictifs nécessaires pour le test
        DateSondeeDto dto = new DateSondeeDto();
        when(modelMapper.map(dto, DateSondee.class)).thenReturn(new DateSondee());
        when(dateSondeeService.create(anyLong(), anyLong(), any(DateSondee.class))).thenReturn(null);

        // Test de la méthode createParticipation du controller
        ResponseEntity<DateSondeeDto> result = dateSondageController.createParticipation(1L, dto);

        // Vérification que les méthodes correspondantes du service et du modelMapper ont été appelées
        verify(dateSondeeService, times(1)).create(eq(1L), eq(dto.getParticipant()), any(DateSondee.class));
        verify(modelMapper, times(1)).map(any(), eq(DateSondeeDto.class));

        // Vérification du résultat
        assertNotNull(result);
        assertEquals(HttpStatus.CREATED, result.getStatusCode()); // Vérifiez le statut HTTP
        assertNull(result.getBody()); // Vérifiez que le corps de la réponse est null
    }
}
