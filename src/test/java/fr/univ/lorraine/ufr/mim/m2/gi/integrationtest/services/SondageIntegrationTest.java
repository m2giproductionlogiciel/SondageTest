package fr.univ.lorraine.ufr.mim.m2.gi.integrationtest.services;

import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.MySurveyApplication;
import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.exceptions.BadRequestException;
import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.exceptions.NotFoundException;
import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.models.Participant;
import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.models.Sondage;
import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.repositories.ParticipantRepository;
import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.repositories.SondageRepository;
import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.services.SondageService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static org.junit.Assert.*;

@SpringBootTest(classes = MySurveyApplication.class)
@AutoConfigureMockMvc
@TestPropertySource(locations = "classpath:application-integrationtest.properties")
@Transactional
class SondageIntegrationTest {

    @Autowired
    private SondageRepository sondageRepository;
    @Autowired
    private ParticipantRepository participantRepository;
    @Autowired
    private SondageService sondageService;

    private long notExistingId = 99L;

    //region Create
    @Test
    void GivenAParticipant_whenCreateSondage_thenCheckSondageIsValid() throws BadRequestException, NotFoundException {
        // Given
        Participant participant = new Participant();
        participant.setNom("Korpys");
        participant.setPrenom("Gaetan");
        participant = participantRepository.save(participant);

        String sondageName = "Test Survey";
        String sondageDesc = "This is a test survey";

        //When
        Sondage sondage = new Sondage();
        sondage.setNom(sondageName);
        sondage.setDescription(sondageDesc);

        sondage = sondageService.create(participant.getParticipantId(), sondage);

        // Then
        assertNotNull(sondage.getSondageId());
        assertEquals(sondage.getNom(), sondageName);
        assertEquals(sondage.getDescription(), sondageDesc);
        assertEquals(sondage.getCreateBy().getParticipantId(), participant.getParticipantId());

        System.out.println("\n" + sondage + "\n");
    }

    @Test
    void GivenInvalidParticipantId_whenCreateSondage_thenBadRequestExceptionIsThrown() {

        // Test with null participant ID
        assertThrows(BadRequestException.class, () -> sondageService.create(null, new Sondage()));

        // Test with participant ID equal to 0
        assertThrows(BadRequestException.class, () -> sondageService.create(0L, new Sondage()));
    }

    @Test
    void GivenNotFoundParticipantId_whenCreateSondage_thenNotFoundtExceptionIsThrown() {

        // Test with participant ID not found in the database
        Long nonExistingParticipantId = notExistingId;
        assertThrows(NotFoundException.class, () -> sondageService.create(nonExistingParticipantId, new Sondage()));
    }

    //endregion

    //region GetAll
    @Test
    void given2Sondage_whenGetAllSondage_thenCheckListSondage() throws BadRequestException, NotFoundException {
        // Given
        Participant participant = new Participant();
        participant.setNom("Korpys");
        participant.setPrenom("Gaetan");
        participant = participantRepository.save(participant);

        Participant participant2 = new Participant();
        participant2.setNom("Rousseau");
        participant2.setPrenom("Théo");
        participant2 = participantRepository.save(participant2);

        String sondageName = "Test Survey";
        String sondageDesc = "This is a test survey";

        String sondageName2 = "Test Survey 2";
        String sondageDesc2 = "This is a test survey 2";


        Sondage sondage = new Sondage();
        sondage.setNom(sondageName);
        sondage.setDescription(sondageDesc);

        Sondage sondage2 = new Sondage();
        sondage2.setNom(sondageName2);
        sondage2.setDescription(sondageDesc2);

        // Starting size :
        int listSize = sondageService.getAll().size();

        sondage = sondageService.create(participant.getParticipantId(), sondage);
        sondage2 = sondageService.create(participant2.getParticipantId(), sondage2);

        //When
        List<Sondage> lstSondage = sondageService.getAll();

        // Then
        assertNotNull(lstSondage);
        assertEquals(listSize + 2, lstSondage.size());  // Ajout de 2 à la liste car 2 éléments crées
        assertEquals(sondage.getCreateBy().getParticipantId(), participant.getParticipantId());
        assertEquals(sondage2.getCreateBy().getParticipantId(), participant2.getParticipantId());

        System.out.println("\n" + lstSondage + "\n");
    }
    //endregion

    //region GetById
    @Test
    void GivenASondageId_whenGetSondageById_thenCheckSondage() throws BadRequestException, NotFoundException {
        // Given
        Participant participant = new Participant();
        participant.setNom("Korpys");
        participant.setPrenom("Gaetan");
        participant = participantRepository.save(participant);

        String sondageName = "Test Survey";
        String sondageDesc = "This is a test survey";

        Sondage sondage = new Sondage();
        sondage.setNom(sondageName);
        sondage.setDescription(sondageDesc);

        sondageService.create(participant.getParticipantId(), sondage);

        // When
        Sondage result = sondageService.getById(sondage.getSondageId());

        // Then
        assertNotNull(result);
        assertEquals(result.getNom(), sondageName);
        assertEquals(result.getDescription(), sondageDesc);
        assertEquals(result.getCreateBy().getParticipantId(), participant.getParticipantId());

        System.out.println("\n" + result + "\n");
    }

    @Test
    void GivenInvalidSondageId_whenGetSondageById_thenBadRequestExceptionIsThrown() {
        // Test with null participant ID
        assertThrows(BadRequestException.class, () -> sondageService.getById(null));

        // Test with participant ID equal to 0
        assertThrows(BadRequestException.class, () -> sondageService.getById(0L));
    }

    @Test
    void GivenNotFoundSondageId_whenGetSondageById_thenNotFoundExceptionIsThrown() {
        // Test with Sondage ID not found in the database
        Long nonExistingSondageId = notExistingId;
        assertThrows(NotFoundException.class, () -> sondageService.getById(nonExistingSondageId));
    }
    //endregion

    //region Update
    @Test
    void GivenSondageId_whenUpdateSondage_thenCheckUpdatedSondage() throws BadRequestException, NotFoundException {
        // Given
        Participant participant = new Participant();
        participant.setNom("Korpys");
        participant.setPrenom("Gaetan");
        participant = participantRepository.save(participant);

        Participant participant2 = new Participant();
        participant2.setNom("Vautrin");
        participant2.setPrenom("Guillaume");
        participant2 = participantRepository.save(participant2);

        String sondageName = "Test Survey";
        String sondageDesc = "This is a test survey";

        Sondage sondage = new Sondage();
        sondage.setNom(sondageName);
        sondage.setDescription(sondageDesc);

        sondage = sondageService.create(participant.getParticipantId(), sondage);

        // When
        String updatedName = "Updated Survey";
        sondage.setNom(updatedName);
        sondage.setCreateBy(participant2);
        Sondage updatedSondage = sondageService.update(sondage.getSondageId(), sondage);

        // Then
        assertNotNull(updatedSondage);
        assertEquals(updatedSondage.getNom(), updatedName);
        assertEquals(updatedSondage.getDescription(), sondageDesc);
        assertEquals(updatedSondage.getCreateBy().getParticipantId(), participant2.getParticipantId());

        System.out.println("\nUpdated Sondage: " + updatedSondage + "\n");
    }

    @Test
    void GivenInvalidSondageId_whenUpdateSondage_thenBadRequestExceptionIsThrown() {
        // Test with null participant ID
        assertThrows(BadRequestException.class, () -> sondageService.update(null, new Sondage()));

        // Test with participant ID equal to 0
        assertThrows(BadRequestException.class, () -> sondageService.update(0L, new Sondage()));
    }

    @Test
    void GivenNotFoundSondageId_whenUpdateSondage_thenNotFoundExceptionIsThrown() {
        // Test with participant ID not found in the database
        Long nonExistingSondageId = notExistingId;
        assertThrows(NotFoundException.class, () -> sondageService.update(nonExistingSondageId, new Sondage()));
    }

    //endregion

    //region Delete
    @Test
    void GivenSondageId_whenDeleteSondage_thenCheckIfSondageIsDeleted() throws BadRequestException, NotFoundException {
        // Given
        Participant participant = new Participant();
        participant.setNom("Korpys");
        participant.setPrenom("Gaetan");
        participant = participantRepository.save(participant);

        String sondageName = "Test Survey";
        String sondageDesc = "This is a test survey";

        Sondage sondage = new Sondage();
        sondage.setNom(sondageName);
        sondage.setDescription(sondageDesc);

        sondageService.create(participant.getParticipantId(), sondage);

        // When
        Long sondageId = sondage.getSondageId();
        int deletionResult = sondageService.delete(sondageId);

        // Then
        assertEquals(1, deletionResult);

        // Verify that the sondage with the given id is not present
        assertFalse(sondageRepository.findById(sondageId).isPresent());

        System.out.println("\nSondage with ID " + sondageId + " deleted successfully.\n");
    }

    @Test
    void GivenInvalidSondageId_whenDeleteSondage_thenBadRequestExceptionIsThrown() {
        // Test with null participant ID
        assertThrows(BadRequestException.class, () -> sondageService.delete(null));

        // Test with participant ID equal to 0
        assertThrows(BadRequestException.class, () -> sondageService.delete(0L));
    }

    @Test
    void GivenNotFoundSondageId_whenDeleteSondage_thenNotFoundExceptionIsThrown() {
        // Test with participant ID not found in the database
        Long nonExistingSondageId = notExistingId;
        assertThrows(NotFoundException.class, () -> sondageService.delete(nonExistingSondageId));
    }
    //endregion

}
