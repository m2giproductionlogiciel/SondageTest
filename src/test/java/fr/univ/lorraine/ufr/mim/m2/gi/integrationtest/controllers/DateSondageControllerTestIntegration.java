package fr.univ.lorraine.ufr.mim.m2.gi.integrationtest.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.controllers.DateSondageController;
import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.dtos.DateSondeeDto;
import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.models.DateSondee;
import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.services.DateSondeeService;
import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.services.DateSondageService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.modelmapper.ModelMapper;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.Date;

import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@ExtendWith(SpringExtension.class)
public class DateSondageControllerTestIntegration {

    @Mock
    private DateSondageService dateSondageService;

    @Mock
    private DateSondeeService dateSondeeService;

    @Mock
    private ModelMapper modelMapper;

    @InjectMocks
    private DateSondageController dateSondageController;

    private final ObjectMapper objectMapper = new ObjectMapper();

    @Test
    void givenValidData_whenCreateParticipation_thenReturnCreated() throws Exception {
        // Créer un objet DateSondeeDto pour le test
        DateSondeeDto dateSondeeDto = new DateSondeeDto();
        dateSondeeDto.setParticipant(1L);

        // Configurer le comportement des mocks
        DateSondee dateSondee = new DateSondee();
        when(modelMapper.map(any(DateSondeeDto.class), eq(DateSondee.class))).thenReturn(dateSondee);
        when(dateSondeeService.create(anyLong(), anyLong(), any(DateSondee.class))).thenReturn(dateSondee);

        // Effectuer la requête POST
        MockMvc mockMvc = MockMvcBuilders.standaloneSetup(dateSondageController).build();
        mockMvc.perform(post("/api/date/{id}/participer", 1L)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(dateSondeeDto)))
                .andExpect(status().isCreated());
    }

    @Test
    void givenInvalidData_whenCreateParticipationWithInvalidData_thenExpectCreated() throws Exception {
        // Créer un objet DateSondeeDto avec des données invalides pour le test
        DateSondeeDto dateSondeeDto = new DateSondeeDto();

        // Configurer le comportement des mocks
        when(modelMapper.map(any(DateSondeeDto.class), eq(DateSondee.class))).thenReturn(new DateSondee());
        when(dateSondeeService.create(anyLong(), anyLong(), any(DateSondee.class))).thenReturn(null);  // Simulez un retour null

        // Effectuer la requête POST
        MockMvc mockMvc = MockMvcBuilders.standaloneSetup(dateSondageController).build();
        mockMvc.perform(post("/api/date/{id}/participer", 1L)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(dateSondeeDto)))
                .andExpect(status().isCreated())
                .andExpect(content().string("")) // Ajoutez cette ligne pour vous assurer que le corps est vide
                .andDo(print()); // Ajoutez cette ligne pour imprimer des informations détaillées sur la requête et la réponse
    }



    @Test
    void givenValidDateId_whenDeleteDate_thenReturnOk() throws Exception {
        // Configurer le comportement des mocks
        doReturn(1).when(dateSondageService).delete(anyLong());

        // Effectuer la requête DELETE
        MockMvc mockMvc = MockMvcBuilders.standaloneSetup(dateSondageController).build();
        mockMvc.perform(delete("/api/date/{id}", 1L))
                .andExpect(status().isOk());
    }


    // Ajouter d'autres tests

    // ...
}
