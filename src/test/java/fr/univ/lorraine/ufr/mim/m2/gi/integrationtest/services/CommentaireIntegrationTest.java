package fr.univ.lorraine.ufr.mim.m2.gi.integrationtest.services;

import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.MySurveyApplication;
import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.exceptions.BadRequestException;
import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.exceptions.NotFoundException;
import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.models.Commentaire;
import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.models.Participant;
import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.models.Sondage;
import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.repositories.ParticipantRepository;
import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.services.CommentaireService;
import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.services.SondageService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.transaction.annotation.Transactional;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest(classes = MySurveyApplication.class)
@AutoConfigureMockMvc
@TestPropertySource(locations = "classpath:application-integrationtest.properties")
@Transactional
class CommentaireIntegrationTest {

    @Autowired
    private CommentaireService commentaireService;

    @Autowired
    private SondageService sondageService;

    @Autowired
    private ParticipantRepository participantService;

    private Participant savedParticipant;
    private Sondage savedSondage;
    private Commentaire savedCommentaire;

    @BeforeEach
    public void setUp() throws BadRequestException, NotFoundException {
        Participant participant = new Participant();
        participant.setParticipantId(1L);
        participant.setNom("Rousseau");
        participant.setPrenom("Théo");
        savedParticipant = participantService.save(participant);

        Sondage sondage = new Sondage();
        sondage.setNom("Test Survey");
        sondage.setDescription("This is a test survey");
        savedSondage = sondageService.create(savedParticipant.getParticipantId(), sondage);

        Commentaire commentaire = new Commentaire();
        commentaire.setCommentaire("Test");
        commentaire.setParticipant(participant);
        commentaire.setSondage(sondage);
        savedCommentaire = commentaireService.addCommantaire(savedSondage.getSondageId(), savedParticipant.getParticipantId(), commentaire);
    }

    @Test
    void GivenCommentaire_WhenCreated_ThenCorrectlyCreated() {
        assertEquals("Test", savedCommentaire.getCommentaire());
        assertEquals(savedSondage, savedCommentaire.getSondage());
        assertEquals(savedParticipant, savedCommentaire.getParticipant());
    }

    @Test
    void GivenCommentaire_WhenUpdated_ThenCorrectlyUpdated() throws BadRequestException, NotFoundException {

        savedCommentaire.setCommentaire("Nouveau test");
        commentaireService.update(savedSondage.getSondageId(), savedParticipant.getParticipantId(),  savedCommentaire);

        assertEquals("Nouveau test", savedCommentaire.getCommentaire());
    }

    @Test
    void GivenCommentaire_WhenDeleted_ThenCorrectlyDeleted() {

        int oldSize = commentaireService.getBySondageId(savedSondage.getSondageId()).size();
        commentaireService.delete(savedCommentaire.getCommentaireId());
        assertEquals(oldSize - 1, commentaireService.getBySondageId(savedSondage.getSondageId()).size());
    }

}