package fr.univ.lorraine.ufr.mim.m2.gi.unittest.models;

import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.models.Choix;
import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.models.DateSondage;
import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.models.DateSondee;
import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.models.Participant;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class DateSondeeTest {

    private DateSondee dateSondee;

    private DateSondage dateSondage;

    private Participant participant;

    private Choix choix;

    private final Long id = 1L;


    @BeforeEach
    void setUp() {
        // Réinitilisation des paramètres
        participant = new Participant();
        dateSondage = new DateSondage();
        choix = Choix.DISPONIBLE;

        // Setters
        dateSondee = new DateSondee();
        dateSondee.setParticipant(participant);
        dateSondee.setDateSondage(dateSondage);
        dateSondee.setChoix(choix.toString());
    }

    @Test
    void GivenDateSondeeWhenGetParticipantThenSameObject() {
        Assertions.assertEquals(participant, dateSondee.getParticipant(), "Le participant devrait être le même");
    }

    @Test
    void GivenDateSondeeWhenGetDateSondageThenSameObject() {
        Assertions.assertEquals(dateSondage, dateSondee.getDateSondage(), "La date de sondage devrait être la même.");
    }

    @Test
    void GivenDateSondeeWhenGetChoixThenSameObject() {
        Assertions.assertEquals(choix.toString(), dateSondee.getChoix(), "Le choix devrait être identique.");
    }

    @Test
    void GivenDateSondeeWhenGetParticipantThenDifferentObject() {
        Assertions.assertEquals(new Participant(), dateSondee.getParticipant(), "Le participant devrait être le même");
    }

    @Test
    void GivenDateSondeeWhenGetDateSondageThenDifferentObject() {
        Assertions.assertNotEquals(new DateSondage(), dateSondee.getDateSondage(), "La date de sondage devrait être la même.");
    }

    @Test
    void GivenDateSondeeWhenGetChoixThenDifferentObject() {
        Assertions.assertNotEquals(Choix.INDISPONIBLE.toString(), dateSondee.getChoix(), "Le choix devrait être identique.");
    }

}