package fr.univ.lorraine.ufr.mim.m2.gi.endtoendtest;

import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.MySurveyApplication;
import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.dtos.SondageDto;
import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.exceptions.BadRequestException;
import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.exceptions.NotFoundException;
import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.models.Participant;
import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.models.Sondage;
import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.repositories.SondageRepository;
import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.services.ParticipantService;
import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.services.SondageService;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.TestPropertySource;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.*;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, classes = MySurveyApplication.class)
@TestPropertySource(locations = "classpath:application-integrationtest.properties")
public class SondageControllerEndToEndTest {

    @Autowired
    private ParticipantService participantService;
    @Autowired
    private SondageRepository sondageRepository;
    @Autowired
    private SondageService sondageService;


    private final String NOM = "NOM";
    private final String PRENOM = "PRENOM";
    private final String DESCRIPTION = "DESCRIPTION";
    private final boolean CLOTURE = false;
    private final Date DATE = Date.from(LocalDate.now().atStartOfDay(ZoneId.systemDefault()).toInstant());
    private Long participantId;
    private Long participant2Id;
    private Long sondageId;
    private Long sondage2Id;

    @LocalServerPort
    private int port;

    @BeforeEach
    public void setUp() throws BadRequestException, NotFoundException {

        // -- Création et insertion du Participant n°1
        // -------------------------------------------
        Participant participant = new Participant();
        participant.setNom(NOM);
        participant.setPrenom(PRENOM);
        participant = participantService.create(participant);
        participantId = participant.getParticipantId();

        // -- Création et insertion du Participant n°2
        // -------------------------------------------
        Participant participant2 = new Participant();
        participant2.setNom(NOM);
        participant2.setPrenom(PRENOM);
        participant2 = participantService.create(participant2);
        participant2Id = participant2.getParticipantId();

        // -- Création et insertion du Sondage n°1
        // ---------------------------------------
        Sondage sondage = new Sondage();
        sondage.setNom(NOM);
        sondage.setDescription(DESCRIPTION);
        sondage.setCloture(CLOTURE);
        sondage.setFin(DATE);

        sondageService.create(participant.getParticipantId(), sondage);
        sondageId = sondage.getSondageId();


        // -- Création et insertion du Sondage n°2
        // ---------------------------------------
        Sondage sondage2 = new Sondage();
        sondage2.setNom(NOM);
        sondage2.setDescription(DESCRIPTION);
        sondage2.setCloture(CLOTURE);
        sondage2.setFin(DATE);

        sondageService.create(participant2.getParticipantId(), sondage2);
        sondage2Id = sondage2.getSondageId();

    }




    //region GetById
    @Test
    void GivenASondageId_whenGetSondageById_thenCheckSondage() {
        RestAssured.port = port;

        Response response = given()
                .contentType(ContentType.JSON)
                .get("/api/sondage/{id}", sondageId);

        response.then()
                .statusCode(HttpStatus.OK.value())
                .body("sondageId", equalTo(sondageId.intValue()))
                .body("nom", equalTo(NOM))
                .body("description", equalTo(DESCRIPTION))
                .body("cloture", equalTo(CLOTURE))
                .body("createBy", equalTo(participantId.intValue()))
                .body("fin", equalTo(DATE.getTime()));
    }

    @Test
    void GivenNotFoundSondageId_whenGetSondageById_thenNotFoundExceptionIsThrown() {
        RestAssured.port = port;

        given()
                .contentType(ContentType.JSON)
                .get("/api/sondage/{id}", 99L)
                .then()
                .statusCode(HttpStatus.NOT_FOUND.value());
    }

    @Test
    void GivenInvalidSondageId_whenGetSondageById_thenBadRequestExceptionIsThrown() {
        RestAssured.port = port;

        given()
                .contentType(ContentType.JSON)
                .get("/api/sondage/{id}", 0L)
                .then()
                .statusCode(HttpStatus.BAD_REQUEST.value());
    }
    //endregion

    //region GetAll
    @Test
    void Given2Sondage_whenGetAllSondage_thenCheckListSondage() throws Exception {
        RestAssured.port = port;

        Response response = given()
                .contentType(ContentType.JSON)
                .get("/api/sondage/");

        response.then()
                .statusCode(HttpStatus.OK.value())
                .body("$", hasSize(greaterThanOrEqualTo(2)))
                //.body("[0].sondageId", equalTo(sondageId.intValue()))
                .body("[0].nom", equalTo(NOM))
                .body("[0].description", equalTo(DESCRIPTION))
                .body("[0].cloture", equalTo(CLOTURE))
                //.body("[0].createBy", equalTo(participantId.intValue()))
                //.body("[1].sondageId", equalTo(sondage2Id.intValue()))
                .body("[1].nom", equalTo(NOM))
                .body("[1].description", equalTo(DESCRIPTION))
                .body("[1].cloture", equalTo(CLOTURE));
                //.body("[1].createBy", equalTo(participant2Id.intValue()));
    }
    //endregion

    //region Create
    @Test
    void GivenAParticipant_whenCreateSondage_thenCheckSondageIsValid() {
        RestAssured.port = port;

        String sondageName = "Test Survey";
        String sondageDesc = "This is a test survey";

        Response response = given()
                .contentType(ContentType.JSON)
                .body(new SondageDto(0L, sondageName, sondageDesc, DATE, true, participantId))
                .post("/api/sondage/");

        response.then().statusCode(201);
    }
    @Test
    void GivenInvalidParticipantId_whenCreateSondage_thenBadRequestExceptionIsThrown() {
        RestAssured.port = port;

        String sondageName = "Test Survey";
        String sondageDesc = "This is a test survey";

        Response response = given()
                .contentType(ContentType.JSON)
                .body(new SondageDto(0L, sondageName, sondageDesc, DATE, true, 0L))
                .post("/api/sondage/");

        response.then().statusCode(HttpStatus.BAD_REQUEST.value());
    }

    @Test
    void GivenNotFoundParticipantId_whenCreateSondage_thenNotFoundtExceptionIsThrown() {
        RestAssured.port = port;

        String sondageName = "Test Survey";
        String sondageDesc = "This is a test survey";

        Response response = given()
                .contentType(ContentType.JSON)
                .body(new SondageDto(0L, sondageName, sondageDesc, DATE, true, 99L))
                .post("/api/sondage/");

        response.then().statusCode(HttpStatus.NOT_FOUND.value());
    }
    //endregion

    //region Update
    @Test
    void GivenSondageId_whenUpdateSondage_thenCheckUpdatedSondage() {
        RestAssured.port = port;

        String sondageName = "New Test Survey";
        String sondageDesc = "This is a new test survey";

        Response response = given()
                .contentType(ContentType.JSON)
                .body(new SondageDto(sondageId, sondageName, sondageDesc, DATE, false, participant2Id))
                .put("/api/sondage/{id}", sondageId);

        response.then().statusCode(HttpStatus.OK.value())
                .body("nom", equalTo(sondageName))
                .body("description", equalTo(sondageDesc))
                .body("cloture", equalTo(false))
                .body("createBy", equalTo(participant2Id.intValue()));
    }

    @Test
    void GivenNotFoundSondageId_whenUpdateSondage_thenNotFoundExceptionIsThrown() throws Exception {
        RestAssured.port = port;

        given()
                .contentType(ContentType.JSON)
                .body("{"
                        + "\"nom\":\"Nouveau Nom\","
                        + "\"description\":\"Nouvelle Description\","
                        + "\"fin\":\"2024-01-18\","
                        + "\"cloture\":true,"
                        + "\"createBy\":1"
                        + "}")
                .when()
                .put("/api/sondage/{id}", 99L)
                .then()
                .statusCode(HttpStatus.NOT_FOUND.value());
    }

    @Test
    void GivenInvalidSondageId_whenUpdateSondage_thenBadRequestExceptionIsThrown() {
        RestAssured.port = port;

        given()
                .contentType(ContentType.JSON)
                .body("{"
                        + "\"nom\":\"Nouveau Nom\","
                        + "\"description\":\"Nouvelle Description\","
                        + "\"fin\":\"2024-01-18\","
                        + "\"cloture\":true,"
                        + "\"createBy\":1"
                        + "}")
                .when()
                .put("/api/sondage/{id}", 0)
                .then()
                .statusCode(HttpStatus.BAD_REQUEST.value());
    }
    //endregion

    //region Delete
    @Test
    void GivenSondageId_whenDeleteSondage_thenCheckIfSondageIsDeleted() {
        RestAssured.port = port;

        given()
                .contentType(ContentType.JSON)
                .when()
                .delete("/api/sondage/{id}", sondageId)
                .then()
                .statusCode(HttpStatus.OK.value());
    }

    @Test
    void GivenNotFoundSondageId_whenDeleteSondage_thenNotFoundExceptionIsThrown() {
        RestAssured.port = port;

        given()
                .contentType(ContentType.JSON)
                .when()
                .delete("/api/sondage/{id}", 99L)
                .then()
                .statusCode(HttpStatus.NOT_FOUND.value());
    }

    @Test
    void GivenInvalidSondageId_whenDeleteSondage_thenBadRequestExceptionIsThrown() {
        RestAssured.port = port;

        given()
                .contentType(ContentType.JSON)
                .when()
                .delete("/api/sondage/{id}", 0L)
                .then()
                .statusCode(HttpStatus.BAD_REQUEST.value());
    }
    //endregion
}
