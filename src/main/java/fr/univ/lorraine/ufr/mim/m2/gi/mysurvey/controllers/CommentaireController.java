package fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.controllers;

import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.dtos.CommentaireDto;
import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.exceptions.BadRequestException;
import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.exceptions.NotFoundException;
import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.models.Commentaire;
import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.models.Participant;
import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.services.CommentaireService;
import io.swagger.annotations.Api;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.modelmapper.ModelMapper;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@Api(tags="Commentaire", description = "Gestion des commentaires")
@RestController
@RequestMapping(value = "/api/commentaire")
public class CommentaireController {

    private final CommentaireService service;
    private final ModelMapper mapper;

    public CommentaireController(CommentaireService service, ModelMapper mapper) {
        this.service = service;
        this.mapper = mapper;
    }

    @Operation(summary = "Modifier un commentaire via son identifiant", description = "Permet de modifier un commentaire grâce à son identifiant")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Commentaire mis à jour."),
            @ApiResponse(responseCode = "404", description = "Commentaire introuvable."),
            @ApiResponse(responseCode = "400", description = "Requête malformée. Veuillez fournir le champ commentaire avec des valeurs non nuls."),
    })
    @PutMapping(value = "/{id}")
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public CommentaireDto update(@PathVariable("id") @Parameter(name="id", description = "Id d'un commentaire.") Long id,
                                 @RequestBody @Parameter(name="commentaireDto", description = "Données du commentaire.") CommentaireDto commentaireDto) throws NotFoundException, BadRequestException {
        try {
            var model = mapper.map(commentaireDto, Commentaire.class);
            var result = service.update(id, commentaireDto.getParticipant(), model);
            return mapper.map(result, CommentaireDto.class);
        } catch(IllegalArgumentException exception){
            throw new NotFoundException("Commentaire introuvable.");
        }catch(Exception e){
            throw new BadRequestException("Requête malformée. Veuillez fournir le champ commentaire avec des valeurs non nuls.");
        }
    }

    @Operation(summary = "Supprimer un commentaire via son identifiant", description = "Permet de supprimer un commentaire grâce à son identifiant")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Commentaire supprimé."),
            @ApiResponse(responseCode = "404", description = "Commentaire introuvable."),
            @ApiResponse(responseCode = "400", description = "Requête malformée. Veuillez fournir le champs id avec des valeurs non nuls."),
    })
    @DeleteMapping(value = "/{id}")
    @ResponseStatus(HttpStatus.OK)
    public void delete(@PathVariable("id") @Parameter(name="id", description = "Id d'un commentaire.") Long id) throws NotFoundException, BadRequestException {
        try {
            service.delete(id);
        } catch(IllegalArgumentException exception){
            throw new NotFoundException("Commentaire introuvable.");
        }catch(Exception e){
            throw new BadRequestException("Requête malformée. Veuillez fournir le champs id avec des valeurs non nuls.");
        }
    }
}
