package fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.repositories;

import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.models.DateSondage;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface DateSondageRepository extends JpaRepository<DateSondage, Long> {

    @Query("SELECT c FROM DateSondage c WHERE c.sondage.sondageId = :id")
    List<DateSondage> getAllBySondage(Long id);

    // Ajout de la méthode existsByDateAndSondageId pour test intégration model DateSondage
    @Query("SELECT COUNT(ds) > 0 FROM DateSondage ds WHERE ds.date = :date AND ds.sondage.sondageId = :sondageId")
    boolean existsByDateAndSondageId(@Param("date") Date date, @Param("sondageId") Long sondageId);

    @Query("SELECT ds FROM DateSondage ds WHERE ds.date = :date AND ds.sondage.sondageId = :sondageId")
    List<DateSondage> findByDateAndSondage_SondageId(@Param("date") Date date, @Param("sondageId") Long sondageId);
}
