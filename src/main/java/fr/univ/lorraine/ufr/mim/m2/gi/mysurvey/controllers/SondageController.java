package fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.controllers;

import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.dtos.CommentaireDto;
import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.dtos.DateSondageDto;
import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.dtos.SondageDto;
import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.exceptions.BadRequestException;
import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.exceptions.NotFoundException;
import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.models.Commentaire;
import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.models.DateSondage;
import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.models.Sondage;
import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.services.CommentaireService;
import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.services.DateSondageService;
import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.services.DateSondeeService;
import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.services.SondageService;
import io.swagger.annotations.Api;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.modelmapper.ModelMapper;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Api(tags = "Sondage", description = "Gestion des sondages")
@RestController
@RequestMapping(value = "/api/sondage")
public class SondageController {

    private final SondageService service;
    private final CommentaireService scommentaire;
    private final DateSondageService sdate;
    private final DateSondeeService request;
    private final ModelMapper mapper;

    public SondageController(SondageService service, ModelMapper mapper, CommentaireService c, DateSondageService d, DateSondeeService r) {
        this.service = service;
        this.mapper = mapper;
        this.sdate = d;
        this.scommentaire = c;
        this.request = r;
    }
    @Operation(summary = "Récupère un sondage", description = "Permet de récupérer les données d'un sondage.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Sondage récupéré."),
            @ApiResponse(responseCode = "404", description = "Sondage introuvable."),
            @ApiResponse(responseCode = "400", description = "Requête malformée."),
    })
    @GetMapping(value = "/{id}")
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public SondageDto get(@PathVariable("id") @Parameter(name="id", description = "Id sondage.") Long id) throws BadRequestException, NotFoundException {
        var model = service.getById(id);
        return mapper.map(model, SondageDto.class);
    }

    @Operation(summary = "Récupère la liste des meilleures dates triées", description = "Permet de récupérer la liste des meilleures dates triées selon le sondage.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Dates récupérées."),
            @ApiResponse(responseCode = "404", description = "Sondage introuvable."),
            @ApiResponse(responseCode = "400", description = "Requête malformée."),
    })
    @GetMapping(value = "/{id}/best")
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public List<Date> getBest(@PathVariable("id") @Parameter(name="id", description = "Id sondage.") Long id) {
        return request.bestDate(id);
    }

    @Operation(summary = "Récupère la liste des meilleures dates probables triées", description = "Permet de récupérer la liste des meilleures dates probables triées selon le sondage.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Dates récupérées."),
            @ApiResponse(responseCode = "404", description = "Sondage introuvable."),
            @ApiResponse(responseCode = "400", description = "Requête malformée."),
    })
    @GetMapping(value = "/{id}/maybe")
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public List<Date> getMaybeBest(@PathVariable("id") @Parameter(name="id", description = "Id sondage.") Long id) {
        return request.maybeBestDate(id);
    }

    @Operation(summary = "Récupère la collection de sondage", description = "Permet de récupérer toute la collection de sondages connus.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Sondages récupérées."),
            @ApiResponse(responseCode = "404", description = "Sondage introuvable."),
    })
    @GetMapping(value = "/")
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public List<SondageDto> get() {
        var models = service.getAll();

        return models.stream()
                .map(model -> mapper.map(model, SondageDto.class))
                .collect(Collectors.toList());
    }

    @Operation(summary = "Récupère la liste des commentaires d'un sondage", description = "Permet de récupérer la liste des commentaires selon le sondage sélectionné.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Commentaires récupérés."),
            @ApiResponse(responseCode = "404", description = "Sondage introuvable."),
    })
    @GetMapping(value = "/{id}/commentaires")
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public List<CommentaireDto> getCommentaires(@PathVariable("id") @Parameter(name="id", description = "Id sondage.") Long id) {
        var models = scommentaire.getBySondageId(id);
        return models.stream()
                .map(model -> mapper.map(model, CommentaireDto.class))
                .collect(Collectors.toList());
    }

    @Operation(summary = "Récupère la liste des dates d'un sondage", description = "Permet de récupérer la liste des dates selon le sondage sélectionné.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Dates récupérés."),
            @ApiResponse(responseCode = "404", description = "Sondage introuvable."),
    })
    @GetMapping(value = "/{id}/dates")
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public List<DateSondageDto> getDates(@PathVariable("id") @Parameter(name="id", description = "Id sondage.") Long id) {
        var models = sdate.getBySondageId(id);
        return models.stream()
                .map(model -> mapper.map(model, DateSondageDto.class))
                .collect(Collectors.toList());
    }
    @Operation(summary = "Créer un nouveau sondage", description = "Permet de créer un nouveau sondage.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Création réussie."),
            @ApiResponse(responseCode = "404", description = "Sondage introuvable."),
            @ApiResponse(responseCode = "400", description = "Requête malformée."),
    })
    @PostMapping(value = "/")
    @ResponseStatus(HttpStatus.CREATED)
    @ResponseBody
    public SondageDto create(@RequestBody  @Parameter(name="sondage", description = "Nouveau sondage") SondageDto sondageDto)
            throws BadRequestException, NotFoundException {

                var model = mapper.map(sondageDto, Sondage.class);
                var result = service.create(sondageDto.getCreateBy(), model);
                return mapper.map(result, SondageDto.class);


    }
    @Operation(summary = "Créer un nouveau commentaire lié à un sondage", description = "Permet de créer un nouveau commentaire lié à un sondage.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Création réussie."),
            @ApiResponse(responseCode = "404", description = "Sondage introuvable."),
            @ApiResponse(responseCode = "400", description = "Requête malformée."),
    })
    @PostMapping(value = "/{id}/commentaires")
    @ResponseStatus(HttpStatus.CREATED)
    @ResponseBody
    public CommentaireDto createCommantaire(@PathVariable("id") @Parameter(name="id", description = "Id sondage.") Long id,
                                            @RequestBody @Parameter(name="commentaire", description = "Nouveau commentaire lié au sondage") CommentaireDto commantaireDto) throws BadRequestException, NotFoundException {
        var model = mapper.map(commantaireDto, Commentaire.class);
        var result = scommentaire.addCommantaire(id, commantaireDto.getParticipant(), model);
        return mapper.map(result, CommentaireDto.class);
    }

    @Operation(summary = "Créer une nouvelle date liée", description = "Permet de créer une nouvelle date liée au sondage sélectionné.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Création réussie."),
            @ApiResponse(responseCode = "404", description = "Sondage introuvable."),
            @ApiResponse(responseCode = "400", description = "Requête malformée."),
    })
    @PostMapping(value = "/{id}/dates")
    @ResponseStatus(HttpStatus.CREATED)
    @ResponseBody
    public DateSondageDto createDate(@PathVariable("id") @Parameter(name="id", description = "Id sondage.") Long id,
                                     @RequestBody @Parameter(name="date de sondage", description = "Nouvelle date de sondage lié au sondage") DateSondageDto dto) throws BadRequestException, NotFoundException {
        var model = mapper.map(dto, DateSondage.class);
        var result = sdate.create(id, model);
        return mapper.map(result, DateSondageDto.class);
    }

    @Operation(summary = "Met à jour un sondage", description = "Permet de mettre à jour le sondage sélectionné.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Mise à jour réussie."),
            @ApiResponse(responseCode = "404", description = "Sondage introuvable."),
            @ApiResponse(responseCode = "400", description = "Requête malformée."),
    })
    @PutMapping(value = "/{id}")
    @ResponseStatus(HttpStatus.OK)
    public SondageDto update(@PathVariable("id") @Parameter(name="id", description = "Id sondage.")Long id,
                             @RequestBody @Parameter(name="sondage", description = "Nouveau contenu du sondage") SondageDto sondageDto) throws BadRequestException, NotFoundException {
        var model = mapper.map(sondageDto, Sondage.class);
        var result = service.update(id, model);
        return mapper.map(result, SondageDto.class);
    }

    @Operation(summary = "Supprime un sondage", description = "Permet de supprimer le sondage sélectionné.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Suppression réussie."),
            @ApiResponse(responseCode = "404", description = "Sondage introuvable."),
            @ApiResponse(responseCode = "400", description = "Requête malformée."),
    })
    @DeleteMapping(value = "/{id}")
    @ResponseStatus(HttpStatus.OK)
    public void delete(@PathVariable("id") @Parameter(name="id", description = "Id sondage.") Long id) throws BadRequestException, NotFoundException {
        service.delete(id);
    }
}
