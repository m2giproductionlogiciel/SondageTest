package fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.dtos;

import io.swagger.annotations.ApiModel;
import io.swagger.v3.oas.annotations.media.Schema;

import java.util.Date;
import java.util.Objects;

@ApiModel("Sondage")
public class SondageDto {

    @Schema(name = "Sondage ID", example = "1", required = true)
    private Long sondageId;

    @Schema(name = "Nom", example = "Titre de sondage", required = true)
    private String nom;

    @Schema(name = "Description", example = "Descriptif du sondage", required = true)
    private String description;
    @Schema(name = "Date de fin", required = true)
    private Date fin;

    @Schema(name = "Cloture", example = "true", required = true)
    private Boolean cloture;

    @Schema(name = "Créer par (Participant ID)", example = "1", required = true)
    private Long createBy;

    public SondageDto() {
    }

    public SondageDto(Long i, String sondageName, String sondageDesc, Date formattedDate, boolean b, Long participantId) {
        sondageId = i;
        nom = sondageName;
        description = sondageDesc;
        fin = formattedDate;
        cloture = b;
        createBy = participantId;
    }

    public Long getSondageId() {
        return sondageId;
    }

    public void setSondageId(Long sondageId) {
        this.sondageId = sondageId;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getFin() {
        return fin;
    }

    public void setFin(Date fin) {
        this.fin = fin;
    }

    public Boolean getCloture() {
        return cloture;
    }

    public void setCloture(Boolean cloture) {
        this.cloture = cloture;
    }

    public Long getCreateBy() {
        return createBy;
    }

    public void setCreateBy(Long createBy) {
        this.createBy = createBy;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof SondageDto)) return false;
        SondageDto that = (SondageDto) o;
        return Objects.equals(getSondageId(), that.getSondageId()) && Objects.equals(getNom(), that.getNom()) && Objects.equals(getDescription(), that.getDescription()) && Objects.equals(getFin(), that.getFin()) && Objects.equals(getCloture(), that.getCloture()) && Objects.equals(getCreateBy(), that.getCreateBy());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getSondageId(), getNom(), getDescription(), getFin(), getCloture(), getCreateBy());
    }
}
