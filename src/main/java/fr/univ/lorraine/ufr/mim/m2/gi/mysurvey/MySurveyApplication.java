package fr.univ.lorraine.ufr.mim.m2.gi.mysurvey;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@SpringBootApplication
@ComponentScan(basePackages = "fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.repositories")
@ComponentScan(basePackages = "fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.services")
@ComponentScan(basePackages = "fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.controllers")
@EnableJpaRepositories
@EnableWebMvc
public class MySurveyApplication {
    public static void main(String[] args) {
        SpringApplication.run(MySurveyApplication.class, args);
    }
}
