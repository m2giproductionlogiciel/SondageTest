package fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.exceptions;

public class BadRequestException extends Exception{
    public BadRequestException(String errorMessage) {
        super(errorMessage);
    }
}
