package fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.services;

import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.exceptions.BadRequestException;
import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.exceptions.NotFoundException;
import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.models.Participant;
import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.repositories.ParticipantRepository;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import java.util.List;

@Service
public class ParticipantService {
    private final ParticipantRepository repository;
    private EntityManager entityManager;

    public ParticipantService(ParticipantRepository repository) {
        this.repository = repository;
    }

    public Participant getById(Long id) throws BadRequestException, NotFoundException {
        if (id == null || id == 0)
            throw new BadRequestException("Participant Id cannot be null or zero.");

        if (repository.findById(id).isPresent())
            return repository.getById(id);
        else
            throw new NotFoundException("Participant with " + id + " not found.");
    }

    public List<Participant> getAll() {
        return repository.findAll();
    }

    public Participant create(Participant participant) {
        return repository.save(participant);
    }

    public Participant update(Long id, Participant participant) {
        if (repository.findById(id).isPresent()) {
            participant.setParticipantId(id);
            return repository.save(participant);
        }
        return null;
    }

    public int delete(Long id) {
        if (repository.findById(id).isPresent()) {
            repository.deleteById(id);
            return 1;
        }
        return 0;
    }
}
