package fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.controllers;

import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.dtos.DateSondeeDto;
import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.exceptions.BadRequestException;
import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.exceptions.NotFoundException;
import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.models.DateSondee;
import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.services.DateSondageService;
import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.services.DateSondeeService;
import io.swagger.annotations.Api;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.modelmapper.ModelMapper;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@Api(tags = "Date de sondage", description = "Gestion des date de sondage")
@RestController
@RequestMapping(value = "/api/date")
public class DateSondageController {

    private final DateSondageService service;
    private final DateSondeeService sds;
    private final ModelMapper mapper;

    public DateSondageController(DateSondageService service, ModelMapper mapper, DateSondeeService s) {
        this.service = service;
        this.mapper = mapper;
        this.sds = s;

    }

    @Operation(summary = "Supprimer une date de sondage via son identifiant", description = "Permet de supprimer une date de sondage grâce à son identifiant")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Suppression effectuée."),
            @ApiResponse(responseCode = "404", description = "Date introuvable."),
            @ApiResponse(responseCode = "400", description = "Requête malformée."),
    })
    @DeleteMapping(value = "/{id}")
    @ResponseStatus(HttpStatus.OK)
    public void delete(@PathVariable("id") @Parameter(name="id", description = "Id date de sondage.") Long id) throws NotFoundException {
        service.delete(id);
    }
    @Operation(summary = "Créer une date de sondage", description = "Permet de créer une nouvelle date de sondage.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Création réussie."),
            @ApiResponse(responseCode = "404", description = "Date introuvable."),
            @ApiResponse(responseCode = "400", description = "Requête malformée."),
    })
    @PostMapping(value = "/{id}/participer")
    @ResponseStatus(HttpStatus.CREATED)
    @ResponseBody
    public ResponseEntity<DateSondeeDto> createParticipation(@PathVariable("id") @Parameter(name="id", description = "Id date de sondage.") Long id,
                                                             @RequestBody @Parameter(name="id", description = "Date de sondage.") DateSondeeDto dto) {
        try {
            var model = mapper.map(dto, DateSondee.class);
            var result = sds.create(id, dto.getParticipant(), model);
            return ResponseEntity.status(HttpStatus.CREATED).body(mapper.map(result, DateSondeeDto.class));
        } catch (BadRequestException e) {
            // Capturer l'exception BadRequestException et renvoyer une réponse avec le code 400
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        } catch (NotFoundException e) {
            // Capturer l'exception NotFoundException et renvoyer une réponse avec le code 404 (ou un autre code approprié)
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
    }
}
