package fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.dtos;

import io.swagger.annotations.ApiModel;
import io.swagger.v3.oas.annotations.media.Schema;

import com.sun.istack.NotNull;

import java.util.Date;
import java.util.Objects;

@ApiModel("Date sondée")
public class DateSondeeDto {

    @Schema(name = "Date sondée ID", example = "1", required = true)
    private Long dateSondeeId;

    @Schema(name = "Participant ID", example = "1", required = true)
    private Long participant;

    @Schema(name="Date sondée", required = true)
    @NotNull
    private Date dateSondee;

    @Schema(name = "Choix", example = "DISPONIBLE", required = true)
    private String choix;

    public DateSondeeDto() {
    }

    public Long getDateSondeeId() {
        return dateSondeeId;
    }

    public void setDateSondeeId(Long dateSondeeId) {
        this.dateSondeeId = dateSondeeId;
    }

    public Long getParticipant() {
        return participant;
    }

    public void setParticipant(Long participant) {
        this.participant = participant;
    }

    public Date getDateSondee() {
        return dateSondee;
    }

    public void setDateSondee(Date dateSondee) {
        this.dateSondee = dateSondee;
    }

    public String getChoix() {
        return choix;
    }

    public void setChoix(String choix) {
        this.choix = choix;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof DateSondeeDto)) return false;
        DateSondeeDto that = (DateSondeeDto) o;
        return Objects.equals(getDateSondeeId(), that.getDateSondeeId()) &&
                Objects.equals(getParticipant(), that.getParticipant()) &&
                Objects.equals(getChoix(), that.getChoix()) &&
                Objects.nonNull(getDateSondee()) &&  // Ajouter une condition non nul
                !getChoix().isEmpty();  // Ajouter une condition non vide
    }

    @Override
    public int hashCode() {
        return Objects.hash(getDateSondeeId(), getParticipant(), getChoix());
    }
}
