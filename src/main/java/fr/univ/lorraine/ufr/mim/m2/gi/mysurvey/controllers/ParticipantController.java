package fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.controllers;

import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.dtos.ParticipantDto;
import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.exceptions.BadRequestException;
import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.exceptions.NotFoundException;
import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.models.Participant;
import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.services.ParticipantService;
import io.swagger.annotations.Api;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.modelmapper.ModelMapper;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@Api(tags = "Participant", description = "Gestion des participants")
@RestController
@RequestMapping(value = "/api/participant")
public class ParticipantController {

    private final ParticipantService service;
    private final ModelMapper mapper;

    public ParticipantController(ParticipantService service, ModelMapper mapper) {
        this.service = service;
        this.mapper = mapper;
    }

    @Operation(summary = "Récupère un participant", description = "Permet de récupérer les données d'un participant.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Participant récupéré."),
            @ApiResponse(responseCode = "404", description = "Participant introuvable."),
            @ApiResponse(responseCode = "400", description = "Requête malformée."),
    })
    @GetMapping(value = "/{id}")
    @ResponseStatus(HttpStatus.OK)
    public ParticipantDto get(@PathVariable("id") @Parameter(name="id", description = "Id participant.") Long id) throws NotFoundException{
        try{
            var model = service.getById(id);
            return mapper.map(model, ParticipantDto.class);
        }catch(Exception exception){
            throw new NotFoundException("Participant introuvable.");
        }
    }
    @Operation(summary = "Récupère une collection de participants", description = "Permet de récupérer les données de tous les participants.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Participants récupérés."),
            @ApiResponse(responseCode = "404", description = "Participants introuvables."),
    })
    @GetMapping(value = "/")
    @ResponseStatus(HttpStatus.OK)
    public List<ParticipantDto> get() {
        var models = service.getAll();
        return models.stream().map(model -> mapper.map(model, ParticipantDto.class)).collect(Collectors.toList());
    }

    @Operation(summary = "Créer un participant", description = "Permet de créer un nouveau participant.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Participant crée."),
            @ApiResponse(responseCode = "400", description = "Requête malformée."),
    })
    @PostMapping(value = "/")
    @ResponseStatus(HttpStatus.CREATED)
    public ParticipantDto create(@RequestBody @Parameter(name="participant", description = "Données d'un participant")ParticipantDto participantDto)
            throws BadRequestException {
        try{
            var model = mapper.map(participantDto, Participant.class);
            var result = service.create(model);
            return mapper.map(result, ParticipantDto.class);
        }catch (Exception e){
            throw new BadRequestException("Requête malformée. Veuillez fournir les champs nom et prenom avec des valeurs non nuls.");
        }

    }

    @Operation(summary = "Modifie un participant", description = "Permet de modifier un participant sélectionné.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Participant mis à jour."),
            @ApiResponse(responseCode = "400", description = "Requête malformée."),
            @ApiResponse(responseCode = "404", description = "Participant introuvable."),
    })
    @PutMapping(value = "/{id}")
    @ResponseStatus(HttpStatus.OK)
    public ParticipantDto update(@PathVariable("id") @Parameter(name="id", description = "Id participant.") Long id,
                                 @RequestBody @Parameter(name="participant", description = "Données à jour d'un participant") ParticipantDto participantDto)
            throws NotFoundException, BadRequestException {
        try{
            var model = mapper.map(participantDto, Participant.class);
            var result = service.update(id, model);
            return mapper.map(result, ParticipantDto.class);
        }catch(IllegalArgumentException exception){
            throw new NotFoundException("Participant introuvable.");
        }catch(Exception e){
            throw new BadRequestException("Requête malformée. Veuillez fournir les champs nom et prenom avec des valeurs non nuls.");
        }
    }

    @Operation(summary = "Supprime un participant", description = "Permet de supprimer un participant sélectionné.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Participant supprimé."),
            @ApiResponse(responseCode = "404", description = "Participant introuvable."),
    })
    @DeleteMapping(value = "/{id}")
    @ResponseStatus(HttpStatus.OK)
    public void delete(@PathVariable("id") @Parameter(name="id", description = "Id participant.") Long id) {
        service.delete(id);
    }
}