    package fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.services;

    import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.exceptions.BadRequestException;
    import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.exceptions.NotFoundException;
    import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.models.DateSondage;
    import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.repositories.DateSondageRepository;
    import org.springframework.stereotype.Service;

    import java.util.List;

    @Service
    public class DateSondageService {

        private final DateSondageRepository repository;
        private final SondageService sondageService;

        public DateSondageService(DateSondageRepository repository, SondageService sondageService) {
            this.repository = repository;
            this.sondageService = sondageService;
        }

//        public DateSondage getById(Long id) {
//            return repository.getById(id);
//        }

        public DateSondage getById(Long id) throws NotFoundException {
            return repository.findById(id).orElseThrow(() -> new NotFoundException("DateSondage with Id " + id + " not found."));
        }

        public List<DateSondage> getBySondageId(Long sondageId) {
            return repository.getAllBySondage(sondageId);
        }

        public DateSondage create(Long id, DateSondage date) throws BadRequestException, NotFoundException {
            date.setSondage(sondageService.getById(id));
            return repository.save(date);
        }

//        public int delete(Long id) {
//            if (repository.findById(id).isPresent()) {
//                repository.deleteById(id);
//                return 1;
//            }
//            return 0;
//        }

        public int delete(Long id) throws NotFoundException {
            if (repository.findById(id).isPresent()) {
                repository.deleteById(id);
                return 1;
            } else {
                throw new NotFoundException("DateSondage with Id " + id + " not found.");
            }
        }
    }
