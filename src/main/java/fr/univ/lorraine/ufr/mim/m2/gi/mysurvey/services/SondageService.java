package fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.services;

import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.exceptions.BadRequestException;
import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.exceptions.NotFoundException;
import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.models.Participant;
import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.models.Sondage;
import fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.repositories.SondageRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SondageService {

    private final SondageRepository repository;
    private final ParticipantService participantService;

    public SondageService(SondageRepository repository, ParticipantService p) {
        this.repository = repository;
        this.participantService = p;
    }

    public Sondage getById(Long id) throws BadRequestException, NotFoundException {

        if(id == null || id == 0)
            throw new BadRequestException("Sondage Id cannot be null or zero.");

        if(repository.findById(id).isPresent())
            return repository.getById(id);
        else
            throw new NotFoundException("Sondage with " + id + " not found.");
    }

    public List<Sondage> getAll() {
        return repository.findAll();
    }

    public Sondage create(Long idParticipant, Sondage sondage) throws BadRequestException, NotFoundException {

        if(idParticipant == null || idParticipant == 0)
            throw new BadRequestException("Participant Id cannot be null or zero.");

        if(sondage == null)
            throw new BadRequestException("Sondage cannot be null.");

        Participant participant = this.participantService.getById(idParticipant);

        if (participant != null)
        {
            sondage.setCreateBy(participant);
            return repository.save(sondage);
        }
        else
            throw new NotFoundException("Participant with Id " + idParticipant + " not found.");

    }

    public Sondage update(Long id, Sondage sondage) throws BadRequestException, NotFoundException {

        if(id == null || id == 0)
            throw new BadRequestException("Sondage Id cannot be null or zero.");

        if(sondage == null)
            throw new BadRequestException("Sondage cannot be null.");

        if (repository.findById(id).isPresent()) {
            sondage.setSondageId(id);
            return repository.save(sondage);
        }
        else
            throw new NotFoundException("Sondage with Id " + id + " not found.");
    }

    public int delete(Long id) throws BadRequestException, NotFoundException {

        if(id == null || id == 0)
            throw new BadRequestException("Sondage Id cannot be null or zero.");

        if (repository.findById(id).isPresent()) {
            repository.deleteById(id);
            return 1;
        }
        else
            throw new NotFoundException("Sondage with Id "+ id + " not found.");
    }
}
