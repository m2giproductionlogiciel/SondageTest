package fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.models;

import javax.persistence.*;

/**
 * Classe de commentaire d'un {@link Sondage}
 */
@Entity
@Table(name = "commentaire")
public class Commentaire {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "commentaire_id")
    private Long commentaireId;

    @Column(name = "commentaire")
    private String commentaire;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "sondage_id")
    private Sondage sondage = new Sondage();

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "participant_id")
    private Participant participant = new Participant();

    public Commentaire() {
    }

    public Long getCommentaireId() {
        return commentaireId;
    }

    public void setCommentaireId(Long commentaireId) {

        if (commentaireId == null || commentaireId < 0) {
            throw new IllegalArgumentException("Le commentaireId ne peut pas être négatif");
        }

        this.commentaireId = commentaireId;
    }

    public String getCommentaire() {
        return commentaire;
    }

    public void setCommentaire(String commentaire) {
        // Vérification si le commentaire est vide ou null
        if (commentaire == null || commentaire.trim().isEmpty()) {
            throw new IllegalArgumentException("Le commentaire ne peut pas être vide ou nul");
        }

        this.commentaire = commentaire;
    }


    public Sondage getSondage() {
        return sondage;
    }

    public void setSondage(Sondage sondage) {
        if (sondage == null) {
            throw new IllegalArgumentException("Le sondage ne peut pas être null");
        }

        this.sondage = sondage;
    }


    public Participant getParticipant() {
        return participant;
    }

    public void setParticipant(Participant participant) {
        if (participant == null) {
            throw new IllegalArgumentException("Le sondage ne peut pas être null");
        }

        this.participant = participant;
    }
}
