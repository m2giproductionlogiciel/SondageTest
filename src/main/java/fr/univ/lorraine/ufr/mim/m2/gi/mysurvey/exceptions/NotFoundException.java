package fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.exceptions;

public class NotFoundException extends Exception {
    public NotFoundException(String errorMessage) {
        super(errorMessage);
    }
}
