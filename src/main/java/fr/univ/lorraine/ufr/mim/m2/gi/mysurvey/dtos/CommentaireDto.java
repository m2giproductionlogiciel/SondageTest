package fr.univ.lorraine.ufr.mim.m2.gi.mysurvey.dtos;

import io.swagger.annotations.ApiModel;
import io.swagger.v3.oas.annotations.media.Schema;

import java.util.Objects;

@ApiModel("Commentaire")
public class CommentaireDto {

    @Schema(name = "Commentaire ID", example = "1", required = true)
    private Long commentaireId;

    @Schema(name = "Commentaire", example = "Je suis un commentaire d'un participant.", required = true)
    private String commentaire;

    @Schema(name = "Participant ID", example = "23", required = true)
    private Long participant;

    public CommentaireDto() {
    }

    public Long getCommentaireId() {
        return commentaireId;
    }

    public void setCommentaireId(Long commentaireId) {
        this.commentaireId = commentaireId;
    }

    public String getCommentaire() {
        return commentaire;
    }

    public void setCommentaire(String commentaire) {
        this.commentaire = commentaire;
    }

    public Long getParticipant() {
        return participant;
    }

    public void setParticipant(Long participant) {
        this.participant = participant;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CommentaireDto)) return false;
        CommentaireDto that = (CommentaireDto) o;
        return Objects.equals(getCommentaireId(), that.getCommentaireId()) && Objects.equals(getCommentaire(), that.getCommentaire()) && Objects.equals(getParticipant(), that.getParticipant());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getCommentaireId(), getCommentaire(), getParticipant());
    }
}
